<?php

/*---------------------------------Start Of FrontEnd--------------------------*/
Route::get('/',function(){
    return view('site.index') ;
});



/*---------------------------------End Of FrontEnd--------------------------*/



/*---------------------------------Start Of DashBoard--------------------------*/

Route::group(['prefix'=>'admin','middleware'=>['auth','Manager','checkRole','smtpAndFcmConfig']],function(){
	/*Start Of DashBoard Controller (Intro Page)*/
	Route::get('dashboard',[
		'uses'  =>'DashBoardController@Index',
		'as'    =>'dashboard',
		'icon'  =>'<i class="icon-home4"></i>',
		'title' =>'الرئيسيه'
		]);


	/*------------ Start Of ContactUsController ----------*/

	#messages page
	Route::get('inbox-page',[
		'uses' =>'ContactUsController@InboxPage',
		'as'   =>'inbox',
		'title'=>'الرسائل',
		'icon' =>'<i class="icon-inbox-alt"></i>',
		'child' =>['showmessage','deletemessage','sendsms','sendemail']
	]);

	#show message page
	Route::get('show-message/{id}',[
		'uses'=>'ContactUsController@ShowMessage',
		'as'  =>'showmessage',
		'title'=>'عرض الرساله'
	]);

	#send sms
	Route::post('send-sms',[
		'uses' =>'ContactUsController@SMS',
		'as'   =>'sendsms',
		'title'=>'ارسال SMS'
	]);

	#send email
	Route::post('send-email',[
		'uses' =>'ContactUsController@EMAIL',
		'as'   =>'sendemail',
		'title'=>'ارسال Email'
	]);

	#delete message
	Route::post('delete-message',[
		'uses' =>'ContactUsController@DeleteMessage',
		'as'   =>'deletemessage',
		'title'=>'حذف الرساله'
	]);

	/*------------ End Of ContactUsController ----------*/

	/*------------ End Of UsersController ----------*/

	#users list
	Route::get('users',[
		'uses' =>'UsersController@Users',
		'as'   =>'users',
		'title'=>'الاعضاء', 
		'icon' =>'<i class="icon-vcard"></i>',
		'child'=>[
			'adduser',
			'updateuser',
			'deleteuser',
			'emailallusers',
			'smsallusers',
			'notificationallusers',
			'sendcurrentemail',
			'sendcurrentsms',
			'sendcurrentnotification'
		]
	]);

	#add user
	Route::post('add-user',[
		'uses' =>'UsersController@AddUser',
		'as'   =>'adduser',
		'title'=>'اضافة عضو'
	]);

	#update user
	Route::post('update-user',[
		'uses' =>'UsersController@UpdateUser',
		'as'   =>'updateuser',
		'title'=>'تحديث عضو'
	]);

	#delete user
	Route::post('delete-user',[
		'uses' =>'UsersController@deleteUser',
		'as'   =>'deleteuser',
		'title'=>'حذف عضو'
	]);

	#email for all users
	Route::post('email-users',[
		'uses' =>'UsersController@EmailMessageAll',
		'as'   =>'emailallusers',
		'title'=>'ارسال email للجميع'
	]);

	#sms for all users
	Route::post('sms-users',[
		'uses' =>'UsersController@SmsMessageAll',
		'as'   =>'smsallusers',
		'title'=>'ارسال sms للجميع'
	]);

	#notification for all users
	Route::post('notification-users',[
		'uses' =>'UsersController@NotificationMessageAll',
		'as'   =>'notificationallusers',
		'title'=>'ارسال notification للجميع'
	]);

	#send email for current user
	Route::post('send-current-email',[
		'uses' =>'UsersController@SendEmail',
		'as'   =>'sendcurrentemail',
		'title'=>'ارساله email لعضو'
	]);

	#send sms for current user
	Route::post('send-current-sms',[
		'uses' =>'UsersController@SendSMS',
		'as'   =>'sendcurrentsms',
		'title'=>'ارساله sms لعضو'
	]);

	#send notification for current user
	Route::post('send-current-notification',[
		'uses' =>'UsersController@SendNotification',
		'as'   =>'sendcurrentnotification',
		'title'=>'ارساله notification لعضو'
	]);
	/*------------ End Of UsersController ----------*/

	/*------------ Start Of ReportsController ----------*/

	#reports page
	Route::get('reports-page',[
		'uses' =>'ReportsController@ReportsPage',
		'as'   =>'reportspage',
		'title'=>'التقارير',
		'icon' =>'<i class=" icon-flag7"></i>',
		'child'=>['deleteusersreports','deletesupervisorsreports']
	]);

	#delete users reports
	Route::post('delete-users-reporst',[
		'uses' =>'ReportsController@DeleteUsersReports',
		'as'   =>'deleteusersreports',
		'title'=>'حذف تقارير الاعضاء'
	]);

	#delete supervisors reports
	Route::post('delete-supervisors-reporst',[
		'uses' =>'ReportsController@DeleteSupervisorsReports',
		'as'   =>'deletesupervisorsreports',
		'title'=>'حذف تقارير المشرفين'
	]);
	/*------------ End Of ReportsController ----------*/

	/*------------ start Of PermissionsController ----------*/
	#permissions list
	Route::get('permissions-list',[
		'uses' =>'PermissionsController@PermissionsList',
		'as'   =>'permissionslist',
		'title'=>'قائمة الصلاحيات',
		'icon' =>'<i class="icon-safe"></i>',
		'child'=>[
			'addpermissionspage',
			'addpermission',
			'editpermissionpage',
			'updatepermission',
			'deletepermission'
		]
	]);

	#add permissions page
	Route::get('permissions',[
		'uses' =>'PermissionsController@AddPermissionsPage', 
		'as'   =>'addpermissionspage',
		'title'=>'اضافة صلاحيه',

	]);

	#add permission
	Route::post('add-permission',[
		'uses' =>'PermissionsController@AddPermissions',
		'as'   =>'addpermission',
		'title' =>'تمكين اضافة صلاحيه'
	]);

	#edit permissions page
	Route::get('edit-permissions/{id}',[
		'uses' =>'PermissionsController@EditPermissions',
		'as'   =>'editpermissionpage',
		'title'=>'تعديل صلاحيه'
	]);

	#update permission
	Route::post('update-permission',[
		'uses' =>'PermissionsController@UpdatePermission',
		'as'   =>'updatepermission',
		'title'=>'تمكين تعديل صلاحيه'
	]);

	#delete permission
	Route::post('delete-permission',[
		'uses'=>'PermissionsController@DeletePermission',
		'as'  =>'deletepermission',
		'title' =>'حذف صلاحيه'
	]);

	/*------------ End Of PermissionsController ----------*/

	/*------------ Start Of MoneyAccountsController ----------*/
	Route::get('money-accounts',[
		'uses' =>'MoneyAccountsController@MoneyAccountsPage',
		'as'   =>'moneyaccountspage',
		'icon' =>'<i class="icon-cash3"></i>',
		'title'=>'الحسابات الماليه',
		'child'=>['moneyaccept','moneyacceptdelete','moneydelete']
	]);

	#accept
	Route::post('accept',[
		'uses' =>'MoneyAccountsController@Accept',
		'as'   =>'moneyaccept',
		'title'=>'تأكيد معامله بنكيه',
	]);

	#accept and delete
	Route::post('accept-delete',[
		'uses' =>'MoneyAccountsController@AcceptAndDelete',
		'as'   =>'moneyacceptdelete',
		'title'=>'تأكيد مع حذف',
	]);

	#delete
	Route::post('money-delete',[
		'uses' =>'MoneyAccountsController@Delete',
		'as'   =>'moneydelete',
		'title'=>'حذف معامله بنكيه',
	]);
	/*------------ End Of MoneyAccountsController ----------*/


	/*------------ Start Of SettingController ----------*/

	#setting page
	Route::get('setting',[
		'uses' =>'SettingController@Setting',
		'as'   =>'setting',
		'title'=>'الاعدادات',
		'icon' =>'<i class="icon-wrench"></i>',
		'child'=>[
			'addsocials',
			'updatesocials',
			'dddd',
			'updatesmtp',
			'updatesms',
			'updateonesignal',
			'updatefcm',
			'updatesitesetting',
			'updateseo',
			'updatesitecopyright',
			'updateemailtemplate',
			'updategoogleanalytics',
			'updatelivechat'
		]
	]);

	#add socials media
	Route::post('add-socials',[
		'uses' =>'SettingController@AddSocial',
		'as'   =>'addsocials',
		'title'=>'اضافة مواقع التواصل'
	]);

	#update socials media
	Route::post('update-socials',[
		'uses' =>'SettingController@UpdateSocial',
		'as'   =>'updatesocials',
		'title'=>'تحديث مواقع التواصل'
	]);

	#delete social
	Route::post('delete-social',[
		'uses' =>'SettingController@DeleteSocial',
		'as'   =>'deletesocial',
		'title'=>'حذف مواقع التاوصل'
	]);

	#update SMTP
	Route::post('update-smtp',[
		'uses' =>'SettingController@SMTP',
		'as'   =>'updatesmtp',
		'title'=>'تحديث SMTP'
	]);

	#update SMS
	Route::post('update-sms',[
		'uses' =>'SettingController@SMS',
		'as'   =>'updatesms',
		'title'=>'تحديث SMS'
	]);

	#update OneSignal
	Route::post('update-onesignal',[
		'uses' =>'SettingController@OneSignal',
		'as'   =>'updateonesignal',
		'title'=>'تحديث OneSignal'
	]);

	#update FCM
	Route::post('update-FCM',[
		'uses' =>'SettingController@FCM',
		'as'   =>'updatefcm',
		'title'=>'تحديث FCM'
	]);

	#update SiteSetting
	Route::post('update-sitesetting',[
		'uses' =>'SettingController@SiteSetting',
		'as'   =>'updatesitesetting',
		'title'=>'تحديث الاعدادات العامه'
	]);

	#update SEO
	Route::post('update-seo',[
		'uses' =>'SettingController@SEO',
		'as'   =>'updateseo',
		'title'=>'تحديث SEO'
	]);

	#update footerCopyRight
	Route::post('update-sitecopyright',[
		'uses' =>'SettingController@SiteCopyRight',
		'as'   =>'updatesitecopyright',
		'title'=>'تحديث حقوق الموقع'
	]);

	#update email template
	Route::post('update-emailtemplate',[
		'uses' =>'SettingController@EmailTemplate',
		'as'   =>'updateemailtemplate',
		'title'=>'تحديث قالب الايميل'
	]);

	#update api google analytics
	Route::post('update-google-analytics',[
		'uses' =>'SettingController@GoogleAnalytics',
		'as'   =>'updategoogleanalytics',
		'title'=>'تحديث google analytics'
	]);

	#update api live chat
	Route::post('update-live-chat',[
		'uses' =>'SettingController@LiveChat',
		'as'   =>'updatelivechat',
		'title'=>'تحديث live chat'
	]);

	/*------------ End Of SettingController ----------*/

    /*------------ Start Of CityController ----------*/
    Route::get('cities',[
        'uses' =>'CityController@cities',
        'as'   =>'cities',
        'icon' =>'<i class="icon-map"></i>',
        'title'=>'قائمة المدن',
        'child'=>['add-city','delete-city','edit-city']
    ]);

    #add
    Route::post('add-city',[
        'uses' =>'CityController@add',
        'as'   =>'add-city',
        'title'=>'اضافة مدينه',
    ]);

    #edit
    Route::post('edit-city',[
        'uses' =>'CityController@edit',
        'as'   =>'edit-city',
        'title'=>'تعديل مدينه',
    ]);

    #delete
    Route::post('delete-city',[
        'uses' =>'CityController@delete',
        'as'   =>'delete-city',
        'title'=>'حذف  مدينه',
    ]);

	/*------------ End Of CityController ----------*/

	/*------------ Start Of CategoryController ----------*/
    Route::get('categories',[
        'uses' =>'CategoryController@categories',
        'as'   =>'categories',
        'icon' =>'<i class="icon-folder-plus"></i>',
        'title'=>'قائمة الاصناف',
        'child'=>['add-category','delete-category','edit-category']
    ]);

    #add
    Route::post('add-category',[
        'uses' =>'CategoryController@add',
        'as'   =>'add-category',
        'title'=>'اضافة صنف جديد',
    ]);

    #edit
    Route::post('edit-category',[
        'uses' =>'CategoryController@edit',
        'as'   =>'edit-category',
        'title'=>'تعديل صنف',
    ]);

    #delete
    Route::post('delete-category',[
        'uses' =>'CategoryController@delete',
        'as'   =>'delete-category',
        'title'=>'حذف  صنف',
    ]);

	/*------------ End Of CategoryController ----------*/
	/*------------ Start Of ActivityController ----------*/
    Route::get('activities',[
        'uses' =>'ActivityController@activities',
        'as'   =>'activities',
        'icon' =>'<i class="icon-calculator"></i>',
        'title'=>'قائمة الانشطه',
        'child'=>['add-activity','delete-activity','edit-activity']
    ]);

    #add
    Route::post('add-activity',[
        'uses' =>'ActivityController@add',
        'as'   =>'add-activity',
        'title'=>'اضافة نشاط جديد',
    ]);

    #edit
    Route::post('edit-activity',[
        'uses' =>'ActivityController@edit',
        'as'   =>'edit-activity',
        'title'=>'تعديل نشاط',
    ]);

    #delete
    Route::post('delete-activity',[
        'uses' =>'ActivityController@delete',
        'as'   =>'delete-activity',
        'title'=>'حذف  نشاط',
    ]);

	/*------------ End Of ActivityController ----------*/
	/*------------ Start Of PropertyController ----------*/
    Route::get('properties',[
        'uses' =>'PropertyController@properties',
        'as'   =>'properties',
        'icon' =>'<i class="icon-calculator"></i>',
        'title'=>'قائمة الخصائص',
        'child'=>['add-property','delete-property','edit-property']
    ]);

    #add
    Route::post('add-property',[
        'uses' =>'PropertyController@add',
        'as'   =>'add-property',
        'title'=>'اضافة  خصيه جديد',
    ]);

    #edit
    Route::post('edit-property',[
        'uses' =>'PropertyController@edit',
        'as'   =>'edit-property',
        'title'=>'تعديل خصيه',
    ]);

    #delete
    Route::post('delete-property',[
        'uses' =>'PropertyController@delete',
        'as'   =>'delete-property',
        'title'=>'حذف  خصيه',
    ]);

	/*------------ End Of PropertyController ----------*/
	
	/*------------ Start Of ShopController ----------*/
    Route::get('shops',[
        'uses' =>'ShopController@shops',
        'as'   =>'shops',
        'icon' =>'<i class="icon-cart"></i>',
        'title'=>'قائمة المطاعم',
		'child'=>[
			'add-shop','delete-shop','edit-shop',
			'shop-products',"add-shop-product","delete-product",
			"edit-shop-product",'edit-shop-product-save',
			'add-addtion-product',"edit-addtion-product",
			'delete-addtion-product',"add-size-product",
			"edit-size-product" ,"delete-size-product"
		]
    ]);

    #add
    Route::post('add-shop',[
        'uses' =>'ShopController@add',
        'as'   =>'add-shop',
        'title'=>'اضافة مطعم جديد',
    ]);

    #edit
    Route::post('edit-shop',[
        'uses' =>'ShopController@edit',
        'as'   =>'edit-shop',
        'title'=>'تعديل مطعم',
    ]);

    #delete
    Route::post('delete-shop',[
        'uses' =>'ShopController@delete',
        'as'   =>'delete-shop',
        'title'=>'حذف  مطعم',
	]);
	
	 #products
	 Route::get('shop-products',[
        'uses' =>'ProductController@products',
        'as'   =>'shop-products',
        'title'=>'قائمة المنتجات',
	]);
	
	#product-add
	Route::post('add-shop-product',[
        'uses' =>'ProductController@add',
        'as'   =>'add-shop-product',
        'title'=>'اضافة منتج',
    ]);
    #product-add
    Route::get('edit-shop-product',[
        'uses' =>'ProductController@edit',
        'as'   =>'edit-shop-product',
        'title'=>'تعديل منتج',
	]);
	#product-delete
    Route::post('delete-product',[
        'uses' =>'ProductController@delete',
        'as'   =>'delete-product',
        'title'=>' حذف منتج',
    ]);
    #product-update
    Route::post('edit-shop-product',[
        'uses' =>'ProductController@update',
        'as'   =>'edit-shop-product-save',
        'title'=>'حقفظ تعديل المنتج',
	]);
	#product-addtion
    Route::post('add-addtion-product',[
        'uses' =>'ProductController@addation',
        'as'   =>'add-addtion-product',
        'title'=>'حفظ الاضاف لمنتج',
	]);
	#product-addtion
    Route::post('edit-addtion-product',[
        'uses' =>'ProductController@editAddation',
        'as'   =>'edit-addtion-product',
        'title'=>'تعديل الاضافه لمنتج',
	]);
	#product-addtion-delete
    Route::post('delete-addtion-product',[
        'uses' =>'ProductController@deleteAddtion',
        'as'   =>'delete-addtion-product',
        'title'=>'حذف الاضافه لمنتج',
	]);
	#product-size-add
    Route::post('add-size-product',[
        'uses' =>'ProductController@addsize',
        'as'   =>'add-size-product',
        'title'=>'اضافة حجم لمنتج',
	]);
	#product-size-edit
    Route::post('edit-size-product',[
        'uses' =>'ProductController@editsize',
        'as'   =>'edit-size-product',
        'title'=>'تعديل حجم لمنتج',
	]);
	#product-size-delete
    Route::post('delete-size-product',[
        'uses' =>'ProductController@deletesize',
        'as'   =>'delete-size-product',
        'title'=>'حذف حجم لمنتج',
    ]);
	
	/*------------ End Of ShopController ----------*/

	/*------------ Start Of AddtionController ----------*/
    Route::get('addtions',[
        'uses' =>'AddtionController@addtions',
        'as'   =>'addtions',
        'icon' =>'<i class="icon-calculator"></i>',
        'title'=>'قائمة الاضافات',
        'child'=>['add-addtion','delete-addtion','edit-addtion']
    ]);

    #add
    Route::post('add-addtion',[
        'uses' =>'AddtionController@add',
        'as'   =>'add-addtion',
        'title'=>'اضافة اضافه جديد',
    ]);

    #edit
    Route::post('edit-addtion',[
        'uses' =>'AddtionController@edit',
        'as'   =>'edit-addtion',
        'title'=>'تعديل اضافه',
    ]);

    #delete
    Route::post('delete-addtion',[
        'uses' =>'AddtionController@delete',
        'as'   =>'delete-addtion',
        'title'=>'حذف  اضافه',
    ]);

	/*------------ End Of AddtionController ----------*/
	
	/*------------ Start Of BankController ----------*/
    Route::get('banks',[
        'uses' =>'BankController@banks',
        'as'   =>'banks',
        'icon' =>'<i class="icon-calculator"></i>',
        'title'=>' الحسابات البنكيه',
        'child'=>['add-bank','delete-bank','edit-bank']
    ]);

    #add
    Route::post('add-bank',[
        'uses' =>'BankController@add',
        'as'   =>'add-bank',
        'title'=>'اضافة حساب جديد',
    ]);

    #edit
    Route::post('edit-bank',[
        'uses' =>'BankController@edit',
        'as'   =>'edit-bank',
        'title'=>'تعديل حساب',
    ]);

    #delete
    Route::post('delete-bank',[
        'uses' =>'BankController@delete',
        'as'   =>'delete-bank',
        'title'=>'حذف  حساب',
    ]);

	/*------------ End Of BankController ----------*/

	/*------------ Start Of MoneyTransferController ----------*/
	Route::get('money-transfers',[
		'uses' =>'MoneyTransferController@MoneyAccountsPage',
		'as'   =>'money.transferspage',
		'icon' =>'<i class="icon-cash3"></i>',
		'title'=>'التحويلات الماليه',
		'child'=>['accept-transfer','transferAccept-delete','transfer-delete']
	]);

	#accept
	Route::post('accept-transfer',[
		'uses' =>'MoneyTransferController@Accept',
		'as'   =>'accept-transfer',
		'title'=>'تأكيد معامله تحوليه',
	]);

	#accept and delete
	Route::post('transferAccept-delete',[
		'uses' =>'MoneyTransferController@AcceptAndDelete',
		'as'   =>'transferAccept-delete',
		'title'=>'تأكيد مع حذف',
	]);

	#delete
	Route::post('transfer-delete',[
		'uses' =>'MoneyTransferController@Delete',
		'as'   =>'transfer-delete',
		'title'=>'حذف تحويلات ',
	]);
	/*------------ End Of MoneyAccountsController ----------*/

	/*------------ End Of OrdersController ----------*/

		#users list
		Route::get('orders',[
			'uses' =>'OrdersController@Orders',
			'as'   =>'orders',
			'title'=>'قائمة الطلبات',
			'subTitle'=> ' الطلبات الحالية ',
			'icon' =>'<i class="glyphicon glyphicon-shopping-cart"></i>',
			'subIcon' =>'<i class="fa fa-cart-plus"></i>',
			'child'=>[
				
				
				'showorder',
				"refuse-orders",
				"new-orders",
				"accept-orders",
				"ready-orders",
				"delivery-orders",
				"reach-orders",
				"action-order"
			]
		]);

		// new orders
		Route::get('new-orders',[
			'uses' =>'OrdersController@newOrders',
			'as'   =>'new-orders',
			'title'=>' طلبات جديده ',
			'icon' =>'<i class="fa fa-truck"></i>',
			'hasFather' => true
		]);
		//  accept
		Route::get('accept-orders',[
			'uses' =>'OrdersController@acceptOrders',
			'as'   =>'accept-orders',
			'title'=>' الطلبات الموافق عليها ',
			'icon' =>'<i class="fa fa-truck"></i>',
			'hasFather' => true
		]);
		//  ready
		Route::get('ready-orders',[
			'uses' =>'OrdersController@readyOrders',
			'as'   =>'ready-orders',
			'title'=>' الطلبات الجاهزه  ',
			'icon' =>'<i class="fa fa-truck"></i>',
			'hasFather' => true
		]);
		//delivery
		Route::get('delivery-orders',[
			'uses' =>'OrdersController@deliveryOrders',
			'as'   =>'delivery-orders',
			'title'=>' طلبات قيدالتوصيل  ',
			'icon' =>'<i class="fa fa-truck"></i>',
			'hasFather' => true
		]);
		//recive
		Route::get('reach-orders',[
			'uses' =>'OrdersController@reachOrders',
			'as'   =>'reach-orders',
			'title'=>'   طلبات استلمت   ',
			'icon' =>'<i class="fa fa-truck"></i>',
			'hasFather' => true
		]);
		//refuse
		Route::get('refuse-orders',[
			'uses' =>'OrdersController@refuseOrders',
			'as'   =>'refuse-orders',
			'title'=>' طلبات مرفوضه   ',
			'icon' =>'<i class="fa fa-truck"></i>',
			'hasFather' => true
		]);
		

		Route::get('show-order/{id}',[
			'uses'=>'OrdersController@ShowOrder',
			'as'  =>'showorder',
			'title'=>'عرض التفاصيل الطلب'
		]);

		Route::get('action-order/{id}/{state}',[
			'uses'=>'OrdersController@actionOrder',
			'as'  =>'action-order',
			'title'=>' تغير حالة الطلب'
		]);

		Route::get('prepare-order/{id}',[
			'uses'=>'OrdersController@PreapretOrder',
			'as'  =>'prepaorder',
			'title'=>'تجهيز الطلب'
		]);

		Route::get('onway-order/{id}',[
			'uses'=>'OrdersController@OnWay',
			'as'  =>'onways',
			'title'=>'قيد الطريق'
		]);

		/*------------ End Of OrdersController ----------*/



});
	Route::get('dd',function(){
		 echo bcrypt(123456);
	});
/*-------------------------------End Of DashBoard--------------------------------*/



//Login Route
Route::get('/login/', 'Auth\LoginController@showLoginForm')->name('login');
Route::post('/login/', 'Auth\LoginController@login');
Route::get('/logout', 'Auth\LoginController@logout')->name('logout');

// Route::get('register', 'Auth\RegisterController@showRegistrationForm');
// Route::post('register','RegisterUserController@Register');
//Route::post('register', 'Auth\RegisterController@register');

// Password Reset Routes
Route::get('password/reset', 'Auth\ForgotPasswordController@showLinkRequestForm')->name('password.request');
Route::post('password/email', 'Auth\ForgotPasswordController@sendResetLinkEmail');
Route::get('password/reset/{token}', 'Auth\ResetPasswordController@showResetForm')->name('password.reset');
Route::post('password/reset', 'Auth\ResetPasswordController@reset');
Route::get('dd', function () {
	 // Insert some stuff
        $user = new App\User;
        $user->name ='اوامر الشبكه';
        $user->last = " ";
        $user->email ='aait@info.com';
        $user->password =bcrypt(111111);
        $user->phone ='123456789';
        $user->avatar ='default.png';
        $user->budget ='100';
		$user->active ='1';
		$user->lng = "36.609486937522895";
		$user->lat = 28.467211723447978;

		$user->role ='1';
		$user->city_id = 1;
        $user->device_id ='1111111111';
        $user->save();
});