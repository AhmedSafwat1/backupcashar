@extends('dashboard.layout.master')

<!-- style -->
@section('style')
    <style type="text/css">
        .modal .icon-camera
        {
            font-size: 100px;
            color: #797979
        }

        .modal input
        {
            margin-bottom: 4px
        }

        .reset
        {
            border:none;
            background: #fff;
            margin-right: 11px;
        }

        .icon-trash
        {
            margin-left: 8px;
            color: red;
        }

        .dropdown-menu
        {
            min-width: 88px;
        }

        #hidden
        {
            display: none;
        }
    </style>
@endsection
<!-- /style -->
@section('content')
    <div class="panel panel-flat">
        <div class="panel-heading">
            <h5 class="panel-title">قائمة الطلبات {{$name}}</h5>
            <div class="heading-elements">
                <ul class="icons-list">
                    <li><a data-action="collapse"></a></li>
                    <li><a data-action="reload"></a></li>
                    <!-- <li><a data-action="close"></a></li> -->
                </ul>
            </div>
        </div>

        <!-- buttons -->
        <div class="panel-body">
            <div class="row">
                <div class="col-xs-4">
                    <button class="btn bg-purple-300 btn-block btn-float btn-float-lg" type="button"><i class="glyphicon glyphicon-shopping-cart"></i> <span>عدد الطلبات  {{$name}} : {{count($orders)}} </span> </button></div>
                <div class="col-xs-4">
                    <a href="{{route('logout')}}" class="btn bg-warning-400 btn-block btn-float btn-float-lg" type="button"><i class="icon-switch"></i> <span>خروج</span></a>
                </div>
            </div>
        </div>
        <!-- /buttons -->

        <table class="table datatable-basic">
            <thead>
            <tr>
                <th> رقم الطلب </th>
                <th> أسم العميل </th>
                <th> رقم الجوال</th>
                <th> العنوان </th>
                <th> مدة الاعداد </th>
                {{-- <th>طريقة الدفع </th> --}}
                <th>ثمن  خدمة التوصيل </th>
                <th> أجمالى السعر </th>
                <th> المطعم</th>
                <th> وقت الشراء </th>
                <th>التفاصيل</th>
            </tr>
            </thead>
            <tbody>
            @foreach($orders as $u)
             @if($u->cards_count > 0)
                <tr>
                    <td>{{$u->id}}</td>
                    <td>{{$u->User->name}}</td>
                    <td>{{$u->User->phone}}</td>
                    <td>{{$u->address}}</td>
                    <td>{{$u->prepared}} دقيقه</td>
                    {{-- <td>{{$u->Payment}}</td> --}}
                    <td>{{$u->delivery_coast}}</td>
                    <td>{{array_sum($u->Cards->pluck('price')->toArray())}} ريال </td>
                    <td>{{$u->Cards()->first()->Size->Product->name_ar}}</td>
                    <td>{{$u->created_at->diffForHumans()}}</td>
                    <td><a href="{{url('admin/show-order/'.$u->id)}}">
                            <button type="button" class="btn btn-primary"> عرض</button>
                        </a></td>
                </tr>
             @endif
            @endforeach
            </tbody>
        </table>

    </div>

    <!-- javascript -->
@section('script')
    <script type="text/javascript" src="{{asset('dashboard/js/plugins/tables/datatables/datatables.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('dashboard/js/plugins/forms/selects/select2.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('dashboard/js/pages/datatables_basic.js')}}"></script>
@endsection

@endsection