@extends('dashboard.layout.master')

<!-- style -->
@section('style')
    <link href="{{asset('dashboard/fileinput/css/fileinput.min.css')}}" rel="stylesheet" type="text/css">
    <link href="{{asset('dashboard/fileinput/css/fileinput-rtl.min.css')}}" rel="stylesheet" type="text/css">
    <link href="{{asset('dashboard/bgrins/spectrum.css')}}" rel='stylesheet' >

    <style>
        .tab-content > .tab-pane {
            display: block;
        }
    </style>

@endsection
<!-- /style -->
@section('content')


    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-flat">
                <div class="panel-heading">
                    <h6 class="panel-title"> رقم الطلب {{ $order->id }} </h6>
                    <div class="heading-elements">
                        <ul class="icons-list">
                        </ul>
                    </div>
                </div>

                <div class="panel-body">
                    <div class="tabbable">
                        <ul class="nav nav-tabs">
                            <li><a href="#basic-tab2" data-toggle="tab"> تفاصيل الطلب </a></li>

                        </ul>

                        <div class="tab-content">
                            <div class="tab-pane" id="basic-tab2">
                                <div class="col-md-12">
                                    <div class="panel panel-flat">
                                        <div class="panel-heading">
                                            <h5 class="panel-title"> بيانات العميل </h5>
                                        </div>

                                        <div class="panel-body">

                                            <table class="table datatable-basic">
                                                <thead>
                                                <tr>
                                                    <th> رقم الطلب  </th>
                                                    <th> الاسم </th>
                                                    <th> رقم الجوال</th>
                                                    <th> البريد الالكترونى </th>
                                                    <th> العنوان </th>
                
                                                    <th> طريقة الدفع </th>
                                                    
                                                </tr>
                                                </thead>
                                                <tbody>
                                                <tr>
                                                    <td>{{$order->id}}</td>
                                                    <td> {{$order->User->name}}</td>
                                                    <td>{{$order->User->phone}}</td>
                                                    <td>{{$order->User->email}}</td>
                                                    <td>{{$order->address}}</td>
                                                    <td>{{$order->Payment}}</td>
                                                   
                                                </tr>
                                                </tbody>
                                            </table>

                                        </div>

                                    </div>
                                </div>

                                <div class="col-md-12">
                                    <div class="panel panel-flat">
                                        <div class="panel-heading">
                                            <h5 class="panel-title"> تفاصيل الطلب </h5>
                                        </div>

                                        <div class="panel-body">
                                            <table class="table datatable-basic">
                                                <thead>
                                                <tr>
                                                    <th> صورة المنتج </th>
                                                    <th> أسم المنتج </th>
                                                    <th> نوع المنتج </th>
                                                    <th> العدد </th>
                                                    <th> السعر </th>
                                                    <th> الحجم</th>
                                         
                                                    <th> أجمالى </th>
                                                </tr>
                                                </thead>
                                                <tbody>

                                                @foreach($order->Cards as $cart)
                                                    <tr>
                                                       
                                                        <td><img src="{{asset('dashboard/uploads/products/'.$cart->Size->Product->image)}}" style="width:40px;height: 40px" class="img-circle" alt=""></td>
                                                        <td>{{$cart->Size->Product->name_ar}}</td>
                                                        <td>{{$cart->Size->Product->Category->name_ar}}</td>
                                                        <td>{{$cart->Size->Product->shop->name_ar}}</td>
                                                        <td>{{$cart->quantity}}</td>
                                                        <td>{{$cart->Size->price}} ريال </td>
                                                        <td>{{$cart->Size->size}}</td>
                    
                                                       <td>{{$cart->quantity * $cart->Size->price}} ريال </td>
                                                   

                                                    </tr>
                                                @endforeach

                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                                {{-- ============== --}}
                                <div class="col-md-12">
                                    <div class="panel panel-flat">
                                        <div class="panel-heading">
                                            <h5 class="panel-title"> تفاصيل الاضافات </h5>
                                        </div>

                                        <div class="panel-body">
                                            <table class="table datatable-basic">
                                                <thead>
                                                <tr>
                                                    <th> اسم الاضافه  </th>
                                                    <th> العدد</th>
                                                    <th> السعر </th>
                                                    <th> اضافه لمنتج </th>
                                                    <th> أجمالى </th>
                                                </tr>
                                                </thead>
                                                <tbody>

                                                @foreach($order->Cards as $cart)
                                                @if($cart->addtions)
                                                @foreach(json_decode($cart->addtions) as $addtion)
                                                    <tr>
                                                       
                                                       
                                                        <td>{{$addtion["name"]}}</td>
                                                        <td>{{$addtion["quantity"]}}</td>
                                                        <td>{{$addtion["price"]}} ريال</td>
                                                        <td>{{$addtion["product_name"]}}</td>
                                
                                                       <td>{{$cart["quantity"] * $addtion["price"]}} ريال </td>
                                                     

                                                    </tr>
                                               
                                                @endforeach
                                                @endif
                                                @endforeach

                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                             
                             <div class="col-md-12">
                                    <div class="panel panel-flat">
                                        <div class="panel-heading">
                                            <h5 class="panel-title"> تفاصيل الطلب</h5>
                                        </div>

                                        <div class="panel-body">
                                            <table class="table datatable-basic">
                                                <thead>
                                                <tr>
                                                    <th> رقم الطلب </th>
                                                    <th> التاريخ  </th>
                                                    <th>ثمن خدمة االتوصيل</th>
                                                    <th> المبلغ المستحق</th>
                                                    <th> الحاله الحاليه</th>
                                                    <th> تغير حالة الطلب </th>
                                                </tr>
                                                </thead>
                                                <tbody>
                                                <tr>
                                                    <td>{{$order['id']}}</td>
                                                    <td>{{$order['created_at']->diffForHumans()}}</td>
        
                                                    <td>{{$order['delivery_coast']}}</td>
                                                    <td>{{array_sum($order->Cards->pluck('total')->toArray())+$order['delivery_coast']}} ريال </td>
                                                    <td>
                                                    @if($order->status == '1')<!--جديد -->
                                                     <span style="color:#5cb85c">جديد</span>
                                                    @elseif($order->status == '2')<!--موافق -->
                                                    <span style="color:#5cb85c"> موافق عليه
                                                    </span>
                                                    @elseif ($order->status == '3')<!--التجهيز -->
                                                    <span style="color:#5cb85c"> قيد التجهيز</span>
                                                    @elseif ($order->status == '4')<!--التوصيل -->
                                                    <span style="color:#5cb85c"> قيد التوصيل
                                                    </span>
                                                    @elseif ($order->status == '5')<!--استلام -->
                                                    <span style="color:#f0ad4e"> تم استلام
                                                    </span>
                                                     @else
                                                     <span style="color:#d9534f"><!--مرفوض -->
                                                      مرفوض </span>
                                                    @endif
                                                    </td>
                                                    
                                
                                                    @if($order->status == '1')
                                                     <th>
                                                         <a href="{{url('admin/action-order/'.$order->id."/2")}}"><button type="button" class="btn btn-info"> الموافقه على  الطلب </button></a>
                                                         <a href="{{url('admin/action-order/'.$order->id."/6")}}"><button type="button" class="btn btn-danger"> رفض  الطلب </button></a>
                                                    </th>
                                                    @elseif($order->status == '2')
                                                    <th><a href="{{url('admin/action-order/'.$order->id."/3")}}"><button type="button" class="btn btn-warning">الطلب جاهز</button></a></th>
                                                    @elseif ($order->status == '3')
                                                    <th><a href="{{url('admin/action-order/'.$order->id."/4")}}"><button type="button" class="btn btn-success"> الطلب قيد التوصيل</button></a></th>
                                                    @elseif ($order->status == '4')
                                                    <th><a href="{{url('admin/action-order/'.$order->id."/5")}}"><button type="button" class="btn btn-primary"> الطلب سلم</button></a></th>
                                        
                                                    @else
                                                    <td> تم الانتهاء </td>
                                                    @endif
                                                </tr>

                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-12">
                                    <div class="panel panel-flat">
                                        <div class="panel-heading">
                                            <h5 class="panel-title"> مكان الطالب على الخريطة  </h5>
                                            <h6> تفاصيل العنوان  : {{$order['address']}} </h6>
                                        </div>

                                        <div class="panel-body">

                                               <table class="table datatable-basic">
                                                <div id="map" style="width:100%; height:250px;"></div>
                                            </table>

                                        </div>

                                    </div>
                                </div> 
                               
                            </div>
                        </div>
                    </div>
                </div>



            </div>
        </div>


        <!-- javascript -->
        @section('script')
            <script src="{{asset('dashboard/bgrins/spectrum.js')}}"></script>

            <script type="text/javascript">
                //stay in current tab after reload
                $(function() {
                    // for bootstrap 3 use 'shown.bs.tab', for bootstrap 2 use 'shown' in the next line
                    $('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
                        // save the latest tab; use cookies if you like 'em better:
                        localStorage.setItem('lastTab', $(this).attr('href'));
                    });

                    // go to the latest tab, if it exists:
                    var lastTab = localStorage.getItem('lastTab');
                    if (lastTab) {
                        $('[href="' + lastTab + '"]').tab('show');
                    }
                });


                $(document).on('change','#color',function(){
                    console.log($(this).val());
                });
            </script>
           

    @endsection
    <!-- /javascript -->
    <script>
        function initMap() {
            
            var myLatLng = {lat:{{ $order->lat}},lng:{{ $order->lng}} };
            var map = new google.maps.Map(document.getElementById('map'), {
                zoom: 18,
                center: myLatLng
            });
            // console.log(map,myLatLng);
            var marker = new google.maps.Marker({
                position: myLatLng,
                map: map,
            });
        }

    </script>
    <script async defer
    src="https://maps.googleapis.com/maps/api/js?key=AIzaSyC5uC_mExFIMSehvCgsjegxcF7mTpKmI4w&callback=initMap">
    </script>

    <script type="text/javascript" src="{{asset('dashboard/fileinput/js/fileinput.min.js')}}"></script>
@endsection