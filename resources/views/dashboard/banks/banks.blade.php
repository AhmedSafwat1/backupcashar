@extends('dashboard.layout.master')

<!-- style -->
@section('style')
    <style type="text/css">
        .modal .icon-camera
        {
            font-size: 100px;
            color: #797979
        }

        .modal input
        {
            margin-bottom: 4px
        }

        .reset
        {
            border:none;
            background: #fff;
            margin-right: 11px;
        }

        .icon-trash
        {
            margin-left: 8px;
            color: red;
        }

        .dropdown-menu
        {
            min-width: 88px;
        }

        #hidden
        {
            display: none;
        }
    </style>
@endsection
<!-- /style -->
@section('content')
    <div class="panel panel-flat">
        <div class="panel-heading">
            <h5 class="panel-title">قائمة الحسابات</h5>
            <div class="heading-elements">
                <ul class="icons-list">
                    <li><a data-action="collapse"></a></li>
                    <li><a data-action="reload"></a></li>
                    <!-- <li><a data-action="close"></a></li> -->
                </ul>
            </div>
        </div>

        <!-- buttons -->
        <div class="panel-body">
            <div class="row">
                <div class="col-xs-4">
                    <button class="btn bg-blue btn-block btn-float btn-float-lg openAddModal" type="button" data-toggle="modal" data-target="#exampleModal"><i class="icon-plus3"></i> <span>اضافة حساب</span></button>
                </div>
                <div class="col-xs-4">
                    <button class="btn bg-purple-300 btn-block btn-float btn-float-lg" type="button"><i class="icon-list-numbered"></i> <span>عدد الحسابات : {{count($banks)}} </span> </button></div>
                <div class="col-xs-4">
                    <a href="{{route('logout')}}" class="btn bg-warning-400 btn-block btn-float btn-float-lg" type="button"><i class="icon-switch"></i> <span>خروج</span></a>
                </div>
            </div>
        </div>
        <!-- /buttons -->

        <table class="table datatable-basic">
            <thead>
            <tr>
                <th>صورة البنك</th>
                <th>البنك باللغه العربيه</th>
                <th> البنك باللغه الانجليزيه</th>
                <th>رقم الحساب</th>
                <th>رقم البيان</th>
                <th>تاريخ الاضافه</th>
                <th>التحكم</th>
            </tr>
            </thead>
            <tbody>
            @foreach($banks as $u)
                <tr>
                    <td><img src="{{asset('dashboard/uploads/banks/'.$u->icon)}}" style="width:40px;height: 40px" class="img-circle" alt=""></td>
                    <td>{{$u->name_ar}}</td>
                    <td>{{$u->name_en}}</td>
                    <td>{{$u->account}} </td>
                    <td>{{$u->iban}}</td>
                    <td>{{$u->created_at->diffForHumans()}}</td>
                    <td>
                        <ul class="icons-list">
                            <li class="dropdown">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                    <i class="icon-menu9"></i>
                                </a>

                                <ul class="dropdown-menu dropdown-menu-right">
                                    <!-- edit button -->
                                    <li>
                                        <a href="#" data-toggle="modal" data-target="#exampleModal2" class="openEditmodal"
                                           data-id="{{$u->id}}"
                                           data-icon="{{$u->icon}}"
                                           data-account="{{$u->account}}"
                                           data-name_ar="{{$u->name_ar}}"
                                           data-name_en="{{$u->name_en}}"
                                           data-iban="{{$u->iban}}"
                                           >
                                            <i class="icon-pencil7"></i>تعديل
                                        </a>
                                    </li>                                
                                    <!-- delete button -->
                                    <form action="{{route('delete-bank')}}" method="POST">
                                        {{csrf_field()}}
                                        <input type="hidden" name="id" value="{{$u->id}}">
                                        <li><button type="submit" class="generalDelete reset"><i class="icon-trash"></i>حذف</button></li>
                                    </form>
                                </ul>
                            </li>
                        </ul>
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>

        <!-- Add banks Modal -->
        <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">أضافة حساب جديد</h5>
                    </div>
                    <div class="modal-body">
                        <div class="row">
                            <form action="{{route('add-bank')}}" method="POST" enctype="multipart/form-data">
                                {{csrf_field()}}
                                <div class="row">
                                    <div class="col-sm-3 text-center">
                                        <label style="margin-bottom: 0">اختيار صورة البنك</label>
                                        <i class="icon-camera"  onclick="addChooseFile()" style="cursor: pointer;"></i>
                                        <div class="images-upload-block">
                                            <input type="file" name="icon" class="image-uploader" id="hidden">
                                        </div>
                                    </div>
                                    <div class="col-sm-9" style="margin-top: 20px">
                                        <div class="row">
                                            <div class="col-md-6">
                                                <label>اسم البنك باللغه العربيه </label>
                                                <input type="text" name="name_ar" class="form-control" placeholder=" اسم البنك باللغه العربيه" style="margin-bottom: 10px">
                                            </div>
                                            <div class="col-md-6">
                                                <label>اسم البنك باللغه الانجليزيه </label>
                                                <input type="text" name="name_en" class="form-control" placeholder="اسم البنك باللغه الانجليزيه" style="margin-bottom: 10px">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                {{-- ======= --}}
                                <div class="row ">
                                    
                                    <div class="col-sm-12 " style="margin-top: 20px">
                                        <label>رقم الحساب  </label>
                                        <input type="text" name="account" class="form-control" placeholder="رقم الحساب " style="margin-bottom: 10px">
                                        <label>رقم الابيان </label>
                                        <input type="text" name="iban" class="form-control" placeholder=" رقم الابيان ">
                                    </div>
                                </div>

                                

                                <div class="col-sm-12" style="margin-top: 10px">
                                    <button type="submit" class="btn btn-primary ">اضافه</button>
                                    <button type="button" class="btn btn-secondary" data-dismiss="modal">أغلاق</button>
                                </div>

                            </form>
                        </div>
                    </div>

                </div>
            </div>
        </div>
        <!-- /Add banks Modal -->

        <!-- Edit banks Modal -->
        <div class="modal fade" id="exampleModal2" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel"> تعديل حساب : <span class="userName"></span> </h5>
                    </div>
                    <div class="modal-body">
                        <form action="{{route('edit-bank')}}" method="post" enctype="multipart/form-data">

                            <!-- token and user id -->
                            {{csrf_field()}}
                            <input type="hidden" name="id" value="">
                            <!-- /token and user id -->
                            <div class="row">
                                    <div class="col-sm-3 text-center">
                                        <label style="margin-bottom: 0">اختيار صورة البنك</label>
                                        <img src="" class="photo" style="width: 120px;height: 120px;cursor: pointer" onclick="ChooseFile()">
                                        <div class="images-upload-block">
                                            <input type="file" name="edit_icon" class="image-uploader" id="hidden">
                                        </div>
                                    </div>
                                    <div class="col-sm-9" style="margin-top: 20px">
                                        <div class="row">
                                            <div class="col-md-6">
                                                <label>اسم البنك باللغه العربيه </label>
                                                <input type="text" name="edit_name_ar" class="form-control" placeholder=" اسم البنك باللغه العربيه" style="margin-bottom: 10px">
                                            </div>
                                            <div class="col-md-6">
                                                <label>اسم البنك باللغه الانجليزيه </label>
                                                <input type="text" name="edit_name_en" class="form-control" placeholder="الاسم باللغه الانجليزيه" style="margin-bottom: 10px">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            <div class="row">
                                <div class="col-sm-12 " style="margin-top: 20px">
                                    <label>رقم الحساب  </label>
                                    <input type="text" name="edit_account" class="form-control" placeholder="رقم الحساب " style="margin-bottom: 10px">
                                    <label>رقم الابيان </label>
                                    <input type="text" name="edit_iban" class="form-control" placeholder=" رقم الابيان ">
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-12" style="margin-top: 10px">
                                    <button type="submit" class="btn btn-primary" >حفظ التعديلات</button>
                                    <button type="button" class="btn btn-secondary" data-dismiss="modal">أغلاق</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <!-- /Edit banks Modal -->

        

    </div>

    <!-- javascript -->
@section('script')
    <script type="text/javascript" src="{{asset('dashboard/js/plugins/tables/datatables/datatables.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('dashboard/js/plugins/forms/selects/select2.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('dashboard/js/pages/datatables_basic.js')}}"></script>
@endsection



<script type="text/javascript">

    $('.openEditmodal').on('click',function(){
        //get valus
        var id                  = $(this).data('id')
        var name_ar             = $(this).data('name_ar')
        var name_en             = $(this).data('name_en')
        var account             = $(this).data('account')
        var iban                = $(this).data('iban')
        var icon                = $(this).data('icon')
        var link = "{{asset('dashboard/uploads/banks/')}}" +'/'+ icon
		$(".photo").attr('src',link)

        //set values in modal inputs
        $("input[name='id']")                  .val(id)
        $("input[name='edit_name_ar']")        .val(name_ar)
        $("input[name='edit_name_en']")        .val(name_en)
        $("input[name='edit_account']")          .val(account)
        $("input[name='edit_iban']")          .val(iban)
        
        

    })

   


</script>

<!-- other code -->
<script type="text/javascript">

    function ChooseFile(){$("input[name='edit_icon']").click()}
    function addChooseFile(){$("input[name='icon']").click()}

    //stay in current tab after reload
    $(function() {
        // for bootstrap 3 use 'shown.bs.tab', for bootstrap 2 use 'shown' in the next line
        $('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
            // save the latest tab; use cookies if you like 'em better:
            localStorage.setItem('lastTab', $(this).attr('href'));
        });

        // go to the latest tab, if it exists:
        var lastTab = localStorage.getItem('lastTab');
        if (lastTab) {
            $('[href="' + lastTab + '"]').tab('show');
        }
    });

</script>
<!-- /other code -->

@endsection 