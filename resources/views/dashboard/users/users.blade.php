
  
@extends('dashboard.layout.master')
	
	<!-- style -->
	@section('style')
		<style type="text/css">
			.modal .icon-camera
			{
				font-size: 100px;
				color: #797979
			}
			.modal input
			{
				margin-bottom: 4px
			}
			.reset
			{
				border:none;
				background: #fff;
    			margin-right: 11px;
			}
			.icon-trash
			{
				margin-left: 8px;
    			color: red;
			}
			.dropdown-menu
			{
				min-width: 88px;
			}
			#hidden
			{
				display: none;
			}
		</style>
	@endsection
	<!-- /style -->
@section('content')
<div class="panel panel-flat">
	<div class="panel-heading">
		<h5 class="panel-title">قائمة الاعضاء</h5>
		<div class="heading-elements">
			<ul class="icons-list">
        		<li><a data-action="collapse"></a></li>
        		<li><a data-action="reload"></a></li>
        		<!-- <li><a data-action="close"></a></li> -->
        	</ul>
    	</div>
	</div>

	<!-- buttons -->
	<div class="panel-body">
		<div class="row">
			<div class="col-xs-3">
				<button class="btn bg-blue btn-block btn-float btn-float-lg openAddModal" type="button" data-toggle="modal" data-target="#exampleModal"><i class="icon-plus3"></i> <span>اضافة عضو</span></button>
			</div>
			<div class="col-xs-3">
				<button class="btn bg-purple-300 btn-block btn-float btn-float-lg" type="button"><i class="icon-list-numbered"></i> <span>عدد الاعضاء : {{count($users)}} </span> </button></div>
			<div class="col-xs-3">
				<button class="btn bg-teal-400 btn-block btn-float btn-float-lg correspondent" type="button" data-toggle="modal" data-target="#exampleModal3" ><i class=" icon-station"></i> <span>مراسلة الاعضاء</span></button></div>
			<div class="col-xs-3">
				<a href="{{route('logout')}}" class="btn bg-warning-400 btn-block btn-float btn-float-lg" type="button"><i class="icon-switch"></i> <span>خروج</span></a>
			</div>
		</div>
	</div>
	<!-- /buttons -->

	<table class="table datatable-basic">
		<thead>
			<tr>
				<th>الصوره</th>
				<th>الاسم</th>
				<th>البريد الالكترونى</th>
				<th>رقم الجوال</th>
				<th>الصلاحيه</th>
				<th>المحفظه</th>
				<th>الحاله</th>
				<th>تاريخ الاضافه</th>
				<th>التحكم</th>
			</tr>
		</thead>
		<tbody>
			@foreach($users as $u)
				<tr>
					<td><img src="{{asset('dashboard/uploads/users/'.$u->avatar)}}" style="width:40px;height: 40px" class="img-circle" alt=""></td>
					<td>{{$u->name}}</td>
					<td>{{$u->email}}</td>
					<td>{{$u->phone}}</td>
					@if(is_null($u->Role) ||isset($u->Role->role) < 1 )
					<td>عضو</td>
					@else
					<td>{{$u->Role->role}}</td>
					@endif
					<td>{{$u->arrears}} ريال</td>
					@if($u->active == 0)
					<td><span class="label label-danger">حظر</span></td>
					@else
					<td><span class="label label-success">نشط</span></td>
					@endif
					<td>{{$u->created_at->diffForHumans()}}</td>
					<td>
					<ul class="icons-list">
						<li class="dropdown">
							<a href="#" class="dropdown-toggle" data-toggle="dropdown">
								<i class="icon-menu9"></i>
							</a>

							<ul class="dropdown-menu dropdown-menu-right">
								<!-- edit button -->
								<li>
									<a href="#" data-toggle="modal" data-target="#exampleModal2" class="openEditmodal" 
									data-id="{{$u->id}}" 
									data-phone="{{$u->phone}}" 
									data-name="{{$u->name}}" 
									data-email="{{$u->email}}"
									data-photo="{{$u->avatar}}"
									data-active="{{$u->active}}"
									data-lng="{{$u->lng}}"
									data-lat="{{$u->lat}}"
									data-last="{{$u->last}}"
									data-address="{{$u->address}}"
									data-city_id="{{$u->city_id}}"
							   	data-birth = "{{$u->birth}}"
									data-permission="{{$u->Role->id}}" >
									<i class="icon-pencil7"></i>تعديل
									</a>
								</li>
								<!-- send message button -->
								<li>
									<a href="#" data-toggle="modal" data-target="#exampleModal4" class="SendMessageUser" 
									data-id="{{$u->id}}"
									data-name="{{$u->name}}"
									data-phone="{{$u->phone}}" 
									data-device_id="{{$u->device_id}}" 
									data-email="{{$u->email}}">
									<i class=" icon-bubble9"></i>مراسله
									</a>
								</li>
								<!-- delete button -->
								<form action="{{route('deleteuser')}}" method="POST">
									{{csrf_field()}}
									<input type="hidden" name="id" value="{{$u->id}}">
									<li><button type="submit" class="generalDelete reset"><i class="icon-trash"></i>حذف</button></li>
								</form>
							</ul>
						</li>
					</ul>
					</td>
				</tr>
			@endforeach
		</tbody>
	</table>

	<!-- Add user Modal -->
	<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
	  <div class="modal-dialog" role="document">
	    <div class="modal-content">
	      <div class="modal-header">
	        <h5 class="modal-title" id="exampleModalLabel">أضافة عضو جديد</h5>
	      </div>
	      <div class="modal-body">
	        <div class="row">
	        	<form action="{{route('adduser')}}" method="POST" enctype="multipart/form-data">
	        		{{csrf_field()}}

	        		<div class="row">
		        		<div class="col-sm-3 text-center">
		        			<label style="margin-bottom: 0">اختيار صوره</label>
		        			<i class="icon-camera"  onclick="addChooseFile()" style="cursor: pointer;"></i>
		        			<div class="images-upload-block">
		        				<input type="file" name="avatar" class="image-uploader" id="hidden">
		        			</div>
		        		</div>
		        		<div class="col-sm-9" style="margin-top: 20px">
										<div class="row">
											<div class="col-md-6">
												<input type="text" name="name" class="form-control" placeholder=" الاسم الاول" style="margin-bottom: 10px">
											</div>
											<div class="col-md-6">
												<input type="text" name="last" class="form-control" placeholder="الاسم الاخير" style="margin-bottom: 10px">
											</div>
											<div class="col-md-12">
												<input type="text" name="email" class="form-control" placeholder="البريد الالكترونى " style="margin-bottom: 10px">
											</div>
										</div>
		        		</div>
	        		</div>

					<div class="row">
		        		<div class="col-sm-6">
		        			<input type="number" name="phone" class="form-control" placeholder="رقم الجوال ">
									<input type="text" name="password" class="form-control" placeholder="الرقم السرى ">
								
									
									<div class="">
										<input type="text" readonly name="address" class="form-control" placeholder=" العنوان">
										<input type="hidden" id="lat" name="lat">
										<input type="hidden" id="lng" name="lng">
									</div>
									<div style="margin-top: 13px">
										<label class="checkbox" style="margin-bottom: 0">
											<label style="padding-right: 0"> حظر</label>
															<input type="checkbox" name="active" value="0"> 
															<i class="icon-checkbox"></i>
											</label>
									</div>
		        		</div>

		        		<div class="col-sm-6">
									<select name="city_id" class=" form-control" id="permissions">
										@foreach($cities as $city)
											<option value="{{$city->id}}">{{$city->code}}</option>
										@endforeach
									</select>
									<div style="margin-top: 3px" >
										<input type="date" name="birth" class="form-control" placeholder="تاريخ الميلاد">
									</div>
									<select name="role" class=" form-control" id="permissions">
										@foreach($roles as $role)
											<option value="{{$role->id}}">{{$role->role}}</option>
										@endforeach
									</select>
									</div>
										
	        		</div>
							<div id="map" style="width:100%;height:200px;margin-top:15px;">
							
							</div>
			        <div class="col-sm-12" style="margin-top: 10px">
				      	<button type="submit" class="btn btn-primary addCategory">اضافه</button>
				        <button type="button" class="btn btn-secondary" data-dismiss="modal">أغلاق</button>
			        </div>

	        	</form>
					</div>
					<div>
						
						
					</div>
	      </div>

	    </div>
	  </div>
	</div>
	<!-- /Add user Modal -->

	<!-- Edit user Modal -->
	<div class="modal fade" id="exampleModal2" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
	  <div class="modal-dialog" role="document">
	    <div class="modal-content">
	      <div class="modal-header">
	        <h5 class="modal-title" id="exampleModalLabel"> تعديل عضو : <span class="userName"></span> </h5>
	      </div>
	      <div class="modal-body">
	        	<form action="{{route('updateuser')}}" method="post" enctype="multipart/form-data">

	        		<!-- token and user id -->
	        		{{csrf_field()}}
	        		<input type="hidden" name="id" value="">
	        		<!-- /token and user id -->
	        		<div class="row">
		        		<div class="col-sm-3 text-center">
		        			<label >اختيار صوره</label>
		        			<img src="" class="photo" style="width: 120px;height: 120px;cursor: pointer" onclick="ChooseFile()">
		        			<input type="file" name="edit_avatar" style="display: none;">
		        		</div>
								<div class="col-sm-9" style="margin-top: 10px">
										<div class="row">
											<div class="col-md-6">
												<label> الاسم الاول</label>
												<input type="text" name="edit_name" class="form-control">
											</div>
											<div class="col-md-6">
												<label> الاسم الثانى</label>
												<input type="text" name="edit_last" class="form-control">
											</div>
											<div class="col-md-12">
												<label>البريد الالكترونى</label>
												<input type="text" name="edit_email" class="form-control">
											</div>
										</div>
									</div>
						</div>
	        		<div class="row ">

		        		<div class="col-sm-6" style="margin-top: 5px">
			        			<label>رقم الجوال</label>
			        			<input type="number" name="edit_phone" class="form-control">
										<label>الرقم السرى</label>
										<input type="text" name="edit_password" class="form-control">
										
										<div class="">
											<label> العنوان</label>
											<input type="text" readonly name="edit_address" class="form-control" placeholder=" العنوان">
											<input type="hidden" id="edit_lat" name="edit_lat">
											<input type="hidden" id="edit_lng" name="edit_lng">
										</div>
									<div style="margin-top: 15px;">
											<label class="checkbox" style="margin-bottom: 0">
												<label style="padding-right: 0"> حظر</label>
																<input type="checkbox" name="active" id="editActive"  value="0"> 
																<i class="icon-checkbox"></i>
															</label>
														</div>
		        		
								
						</div>

						<div class="col-sm-6 " style="margin-top:5px">
						<label> كود البلد</label>
						<select name="edit_city_id"  id="cities" class=" form-control" id="permissions">
								@foreach($cities as $city)
									<option value="{{$city->id}}">{{$city->code}}</option>
								@endforeach
							</select>
							<div style="margin-top: 5px" >
									<label> تاريخ الميلاد</label>
									<input type="date" name="edit_birth" class="form-control" placeholder="تاريخ الميلاد">
								</div>
							<select name="role" class="form-control" id="permissions" style="margin-top: 30px">
								@foreach($roles as $role)
									<option value="{{$role->id}}">{{$role->role}}</option>
								@endforeach
							</select>
							
						</div>
						<div id="map1" style="width:100%;height:200px;margin-top:15px;">
							
						</div>
				      <div class="col-sm-12" style="margin-top: 10px">
				      	<button type="submit" class="btn btn-primary" >حفظ التعديلات</button>
				        <button type="button" class="btn btn-secondary" data-dismiss="modal">أغلاق</button>
				      </div>
			      </div>
	        	</form>
	      </div>
	    </div>
	  </div>
	</div>
	<!-- /Edit user Modal -->

	<!-- correspondent for all users Modal -->
	<div class="modal fade" id="exampleModal3" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
		<div class="modal-dialog" role="documeent">
			<div class="modal-content">
			  <div class="modal-header">
			    <h5 class="modal-title" id="exampleModalLabel">مراسلة جميع الاعضاء</span></h5>
			  </div>
			  <div class="modal-body">
			    <div class="row">
					<div class="tabbable">
						<ul class="nav nav-tabs bg-slate nav-tabs-component nav-justified">
							<!-- email -->
							<li class="active"><a href="#colored-rounded-justified-tab1" data-toggle="tab">ايميل</a></li>
							<!-- sms -->
							<li><a href="#colored-rounded-justified-tab2" data-toggle="tab">رساله SMS</a></li>
							<!-- notification -->
							<li><a href="#colored-rounded-justified-tab3" data-toggle="tab">اشعار</a></li>
						</ul>

						<div class="tab-content">
							<!-- email -->
							<div class="tab-pane active" id="colored-rounded-justified-tab1">
							    <div class="row">
							    	<form action="{{route('emailallusers')}}" method="POST" enctype="multipart/form-data">
							    		{{csrf_field()}}
							    		<div class="col-sm-12">
							    			<textarea rows="15" name="email_message" class="form-control" placeholder="نص رسالة الـ Email "></textarea>
							    		</div>

								        <div class="col-sm-12" style="margin-top: 10px">
									      	<button type="submit" class="btn btn-primary addCategory">ارسال</button>
									        <button type="button" class="btn btn-secondary" data-dismiss="modal">أغلاق</button>
								        </div>
							    	</form>
							    </div>
							</div>
							<!-- sms -->
							<div class="tab-pane" id="colored-rounded-justified-tab2">
							    <div class="row">
							    	<form action="{{route('smsallusers')}}" method="POST" enctype="multipart/form-data">
							    		{{csrf_field()}}
							    		<div class="col-sm-12">
							    			<textarea rows="15" name="sms_message" class="form-control" placeholder="نص رسالة الـ SMS "></textarea>
							    		</div>
								        <div class="col-sm-12" style="margin-top: 10px">
									      	<button type="submit" class="btn btn-primary addCategory">ارسال</button>
									        <button type="button" class="btn btn-secondary" data-dismiss="modal">أغلاق</button>
								        </div>
							    	</form>
							    </div>
							</div>

							<!-- noification -->
							<div class="tab-pane" id="colored-rounded-justified-tab3">
							    <div class="row">
							    	<form action="{{route('sendsms')}}" method="POST" enctype="multipart/form-data">
							    		{{csrf_field()}}
							    		<div class="col-sm-12">
							    			<textarea rows="15" name="sms_message" class="form-control" placeholder="نص رسالة الـ Notification "></textarea>
							    		</div>

								        <div class="col-sm-12" style="margin-top: 10px">
									      	<button type="submit" class="btn btn-primary addCategory">ارسال</button>
									        <button type="button" class="btn btn-secondary" data-dismiss="modal">أغلاق</button>
								        </div>
							    	</form>
							    </div>
							</div>
						</div>
					</div>
			    </div>
			  </div>
			</div>
		</div>
	</div>
	<!-- /correspondent for all users Modal -->
	<!-- correspondent for one user Modal -->
	<div class="modal fade" id="exampleModal4" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
			  <div class="modal-header">
			    <h5 class="modal-title" id="exampleModalLabel">مراسلة :  <span class="reverName"></span></h5>
			  </div>
			  <div class="modal-body">
			    <div class="row">
					<div class="tabbable">
						<ul class="nav nav-tabs bg-slate nav-tabs-component nav-justified">
							<!-- email -->
							<li class="active"><a href="#colored-rounded-justified-tab10" data-toggle="tab">ايميل</a></li>
							<!-- sms -->
							<li><a href="#colored-rounded-justified-tab20" data-toggle="tab">رساله SMS</a></li>
							<!-- notification -->
							<li><a href="#colored-rounded-justified-tab30" data-toggle="tab">اشعار</a></li>
						</ul>
						<div class="tab-content">
							<!-- email -->
							<div class="tab-pane active" id="colored-rounded-justified-tab10">
							    <div class="row">
							    	<form action="{{route('sendcurrentemail')}}" method="POST" enctype="multipart/form-data">
							    		{{csrf_field()}}
							    		<input type="hidden" name="email" value="">
							    		<div class="col-sm-12">
							    			<textarea rows="15" name="email_message" class="form-control" placeholder="نص رسالة الـ Email "></textarea>
							    		</div>
								        <div class="col-sm-12" style="margin-top: 10px">
									      	<button type="submit" class="btn btn-primary addCategory">ارسال</button>
									        <button type="button" class="btn btn-secondary" data-dismiss="modal">أغلاق</button>
								        </div>
							    	</form>
							    </div>
							</div>

							<!-- sms -->
							<div class="tab-pane" id="colored-rounded-justified-tab20">
							    <div class="row">
							    	<form action="{{route('sendcurrentsms')}}" method="POST" enctype="multipart/form-data">
							    		{{csrf_field()}}
							    		<input type="hidden" name="phone" value="">
							    		<div class="col-sm-12">
							    			<textarea rows="15" name="sms_message" class="form-control" placeholder="نص رسالة الـ SMS "></textarea>
							    		</div>

								        <div class="col-sm-12" style="margin-top: 10px">
									      	<button type="submit" class="btn btn-primary addCategory">ارسال</button>
									        <button type="button" class="btn btn-secondary" data-dismiss="modal">أغلاق</button>
								        </div>
							    	</form>
							    </div>
							</div>
							<!-- notification -->
							<div class="tab-pane" id="colored-rounded-justified-tab30">
							    <div class="row">
							    	<form action="{{route('sendcurrentnotification')}}" method="POST" enctype="multipart/form-data">
							    		{{csrf_field()}}
							    		<input type="hidden" name="device_id" value="">
							    		<div class="col-sm-12">
							    			<textarea rows="15" name="sms_message" class="form-control" placeholder="نص رسالة الـ Notification "></textarea>
							    		</div>
								        <div class="col-sm-12" style="margin-top: 10px">
									      	<button type="submit" class="btn btn-primary addCategory">ارسال</button>
									        <button type="button" class="btn btn-secondary" data-dismiss="modal">أغلاق</button>
								        </div>
							    	</form>
							    </div>
							</div>
						</div>
					</div>
			    </div>
			  </div>

			</div>
		</div>
	</div>
	<!-- /correspondent for one user Modal -->

</div>

<!-- javascript -->
@section('script')
<script type="text/javascript" src="{{asset('dashboard/js/plugins/tables/datatables/datatables.min.js')}}"></script>
<script type="text/javascript" src="{{asset('dashboard/js/plugins/forms/selects/select2.min.js')}}"></script>
<script type="text/javascript" src="{{asset('dashboard/js/pages/datatables_basic.js')}}"></script>
@endsection


{{-- edit  --}}

<script type="text/javascript">
	$('.openEditmodal').on('click',function(){
		//get valus 
		var id         = $(this).data('id')
		var name       = $(this).data('name')
		var photo      = $(this).data('photo')
		var phone      = $(this).data('phone')
		var email      = $(this).data('email')
		var permission = $(this).data('permission')
		var active     = $(this).data('active')
		
		var last   		 = $(this).data("last")
		var city_id    = $(this).data("city_id")
		var address    = $(this).data("address")
		var lat        = $(this).data("lat")
		var lng 			 = $(this).data("lng")
		var birth			 = $(this).data("birth")
		console.log(address);
		//set values in modal inputs
		$("input[name='id']")             .val(id)
		$("input[name='edit_name']")      .val(name)
		$("input[name='edit_phone']")     .val(phone)
		$("input[name='edit_email']")     .val(email)
		$("input[name='edit_lat']")     	.val(lat)
		$("input[name='edit_lng']")       .val(lng)
		$("input[name='edit_last']")      .val(last)
		$("input[name='edit_address']")   .val(address)
		$("input[name='edit_birth']")     .val(birth)
		var link = "{{asset('dashboard/uploads/users/')}}" +'/'+ photo
		$(".photo").attr('src',link)
		$('.userName').text(name)
		console.log(permission)
		//select role
		$('#permissions option').each(function(){
			if($(this).val() == permission)
			{
				$(this).attr('selected','')
			}
		});
		$('#cities option').each(function(){
			if($(this).val() == city_id)
			{
				$(this).attr('selected','')
			}
		});
		//block input check
		if(active == 0)
		{
			$('#editActive').attr('checked','')
		}
		mapSetting(lat ,lng)
	})
	//open send message modal
	$('.SendMessageUser').on('click',function(){
		var name     = $(this).data('name');
		var phone    = $(this).data('phone');
		var email    = $(this).data('email');
		var device_id= $(this).data('device_id');
		$('.reverName').html(name);
		$('input[name="phone"]').val(phone);
		$('input[name="email"]').val(email);
		$('input[name="device_id"]').val(device_id);
	})
</script>

<!-- other code -->
<script type="text/javascript">
	function ChooseFile(){$("input[name='edit_avatar']").click()}
	function addChooseFile(){$("input[name='avatar']").click()}
	//stay in current tab after reload
	$(function() { 
	    // for bootstrap 3 use 'shown.bs.tab', for bootstrap 2 use 'shown' in the next line
	    $('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
	        // save the latest tab; use cookies if you like 'em better:
	        localStorage.setItem('lastTab', $(this).attr('href'));
	    });
	    // go to the latest tab, if it exists:
	    var lastTab = localStorage.getItem('lastTab');
	    if (lastTab) {
	        $('[href="' + lastTab + '"]').tab('show');
	    }
	});
</script>
<!-- /other code -->

{{-- code for map  --}}

<script type="text/javascript">
	function initMap() {
			var latlng = new google.maps.LatLng('{{Auth::User()['lat']}}', '{{Auth::User()['lng']}}');
			var map = new google.maps.Map(document.getElementById('map'), {
					center: latlng,
					zoom: 15,
					disableDefaultUI: true,
					mapTypeId: google.maps.MapTypeId.ROADMAP
			});
			var marker = new google.maps.Marker({
					position: latlng,
					animation: google.maps.Animation.DROP,
					map: map,
					draggable: true
			});
			google.maps.event.addListener(marker, 'dragend', function (event) {
					document.getElementById("lat").value = this.getPosition().lat();
					document.getElementById("lng").value = this.getPosition().lng();
					var latitude = roundOf(this.getPosition().lat(), 4);
					var longitude = roundOf(this.getPosition().lng(), 4);
					function roundOf(n, p) {
							const n1 = n * Math.pow(10, p + 1);
							const n2 = Math.floor(n1 / 10);
							if (n1 >= (n2 * 10 + 5)) {
									return (n2 + 1) / Math.pow(10, p);
							}
							return n2 / Math.pow(10, p);
					}
					var urlTo = "https://maps.googleapis.com/maps/api/geocode/json?latlng=" + latitude + "," + longitude + "&key=AIzaSyC5uC_mExFIMSehvCgsjegxcF7mTpKmI4w&sensor=true&language=ar";
					$.ajax({
							url: [urlTo],
							cache: false,
							success: function (data) {
									var address = data.results[0].formatted_address;
									console.log(latitude)
									$("input[name='lat']").val(latitude);
									$("input[name='lng']").val(longitude);
									$("input[name='address']").val(address);
									$('.loc').html(address).fadeIn();
							}
					});
			});
	}
</script>
<!-- edit map setting  !-->
<script type="text/javascript">
	function mapSetting(lat ,lng){
	
			var latlng = new google.maps.LatLng(lat , lng);
			var map = new google.maps.Map(document.getElementById('map1'), {
					center: latlng,
					zoom: 15,
					disableDefaultUI: true,
					zoomControl: true,
					mapTypeId: google.maps.MapTypeId.ROADMAP,
			});
			var marker = new google.maps.Marker({
					position: latlng,
					map: map,
					animation: google.maps.Animation.DROP,
					draggable: true
			});
			google.maps.event.addListener(marker, 'dragend', function (event) {
					document.getElementById("edit_lat").value  = this.getPosition().lat();
					document.getElementById("edit_lng").value  = this.getPosition().lng();
					var latitude  =  roundOf(this.getPosition().lat(),4);
					var longitude =  roundOf(this.getPosition().lng(),4);
					function roundOf(n, p) {
							const n1 = n * Math.pow(10, p + 1);
							const n2 = Math.floor(n1 / 10);
							if (n1 >= (n2 * 10 + 5)) {
									return (n2 + 1) / Math.pow(10, p);
							}
							return n2 / Math.pow(10, p);
					}
					var urlTo = "https://maps.googleapis.com/maps/api/geocode/json?latlng="+latitude+","+longitude+"&key=AIzaSyC5uC_mExFIMSehvCgsjegxcF7mTpKmI4w&sensor=true&language=ar";
					$.ajax({
							url:[urlTo] ,
							cache: false,
							success: function(data){
									var ar = data.results[0].formatted_address;
									$("input[name='lat']").val(latitude);
									$("input[name='lng']").val(longitude);
									$("input[name='edit_address']") .val(ar);
							}
					});
			});
	}
</script>


<script async defer
			src="https://maps.googleapis.com/maps/api/js?key=AIzaSyC5uC_mExFIMSehvCgsjegxcF7mTpKmI4w&callback=initMap">
</script>
 {{-- end --}}

@endsection

