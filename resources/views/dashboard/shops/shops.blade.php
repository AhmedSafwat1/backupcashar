@extends('dashboard.layout.master')
	
	<!-- style -->
	@section('style')
		<style type="text/css">
			.modal .icon-camera
			{
				font-size: 100px;
				color: #797979
			}

			.modal input
			{
				margin-bottom: 4px
			}

			.reset
			{
				border:none;
				background: #fff;
    			margin-right: 11px;
			}

			.icon-trash
			{
				margin-left: 8px;
    			color: red;
			}

			.dropdown-menu
			{
				min-width: 88px;
			}

			#hidden
			{
				display: none;
			}
			.dropdown-menu {
						min-width: 138px;
				}
				.mt-1
				{
					margin-top: 10px;
				}

		</style>
	@endsection
	<!-- /style -->
@section('content')
<div class="panel panel-flat">
	<div class="panel-heading">
		<h5 class="panel-title">قائمة المطعم</h5>
		<div class="heading-elements">
			<ul class="icons-list">
        		<li><a data-action="collapse"></a></li>
        		<li><a data-action="reload"></a></li>
        		<!-- <li><a data-action="close"></a></li> -->
        	</ul>
    	</div>
	</div>

	<!-- buttons -->
	<div class="panel-body">
		<div class="row">
			<div class="col-xs-4">
				<button class="btn bg-blue btn-block btn-float btn-float-lg openAddModal" type="button" data-toggle="modal" data-target="#exampleModal"><i class="icon-plus3"></i> <span>اضافة مطعم</span></button>
			</div>
			<div class="col-xs-4">
				<button class="btn bg-purple-300 btn-block btn-float btn-float-lg" type="button"><i class="icon-list-numbered"></i> <span>عدد المطعم : {{count($shops)}} </span> </button>
			</div>
			<div class="col-xs-4	">
				<a href="{{route('logout')}}" class="btn bg-warning-400 btn-block btn-float btn-float-lg" type="button"><i class="icon-switch"></i> <span>خروج</span></a>
			</div>
		</div>
	</div>
	<!-- /buttons -->

	<table class="table datatable-basic">
		<thead>
			<tr>
				<th>الصوره</th>
				<th>الاسم باللغه العربيه</th>
				<th>الاسم باللغه الانجليزيه</th>
				<th>ميعاد بدء العمل</th>
				<th>ميعاد انتهاء العمل</th>
				<th>خدمة التوصيل</th>
				<th> تكلفة خدمة التوصيل </th>
				<th>تاريخ الاضافه</th>
				<th>التحكم</th>
			</tr>
		</thead>
		<tbody>
			@foreach($shops as $u)
				<tr>
					<td><img src="{{asset('dashboard/uploads/shops/'.$u->image)}}" style="width:40px;height: 40px" class="img-circle" alt=""></td>
					<td>{{$u->name_ar}}</td>
					<td>{{$u->name_en}}</td>
					<td>{{\Carbon::parse($u->start_time)->format("g:i A")}}</td>
					<td>{{ \Carbon::parse($u->end_time)->format("g:i A")}} </td>
					<td>{{$u->delivery}} </td>
					<td>{{$u->delivery_coast}} </td>
					<td>{{$u->created_at->diffForHumans()}}</td>
					<td>
					<ul class="icons-list">
						<li class="dropdown">
							<a href="#" class="dropdown-toggle" data-toggle="dropdown">
								<i class="icon-menu9"></i>
							</a>

							<ul class="dropdown-menu dropdown-menu-right">
								<!-- edit button -->
								<li>
									<a href="#" data-toggle="modal" data-target="#exampleModal2" class="openEditmodal" 
									data-id="{{$u->id}}" 
									data-name_ar="{{$u->name_ar}}" 
									data-name_en="{{$u->name_en}}" 
									data-start_time="{{$u->start_time}}"
									data-end_time="{{$u->end_time}}"
									data-photo="{{$u->image}}"
									data-delivery="{{$u->delivery}}"
									data-lng="{{$u->lng}}"
									data-lat="{{$u->lat}}"
									data-delivery_coast="{{$u->delivery_coast}}"
									data-delivery_postion="{{$u->delivery_postion}}"
									data-desc_ar="{{$u->desc_ar}}"
									data-desc_en="{{$u->desc_en}}"
									data-categories="{{$u->Categories}}"
									data-activities="{{$u->Activities}}"
									data-properties="{{$u->ShopProperties}}"
							         >
									<i class="icon-pencil7"></i>تعديل
									</a>
								</li>				
									<!-- add product -->
								<li>
										<form action="{{route('shop-products')}}" method="get">
												{{-- {{csrf_field()}} --}}
											<input type="hidden" name="shop_id" value="{{$u->id}}">
											<button type="submit" class="reset "><i class="icon-cart" style="margin-left:5px;"></i> قائمة المنتجات</button>
										</form>
							</li>														
								</li>																																																				
								<!-- delete button -->
								<form action="{{route('delete-shop')}}" method="POST" class="mt-1">
									{{csrf_field()}}
									<input type="hidden" name="id" value="{{$u->id}}">
									<li><button type="submit" class="generalDelete reset"><i class="icon-trash"></i>حذف</button></li>
								</form>
							
								
							</ul>
						</li>
					</ul>
					</td>
				</tr>
			@endforeach
		</tbody>
	</table>

	<!-- Add user Modal -->
	<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
	  <div class="modal-dialog" role="document">
	    <div class="modal-content">
	      <div class="modal-header">
	        <h5 class="modal-title" id="exampleModalLabel">أضافة مطعم جديد</h5>
	      </div>
	      <div class="modal-body">
	        <div class="row">
	        	<form action="{{route('add-shop')}}" method="POST" enctype="multipart/form-data">
	        		{{csrf_field()}}

	        		<div class="row">
		        		<div class="col-sm-3 text-center">
		        			<label style="margin-bottom: 0">اختيار صوره</label>
		        			<i class="icon-camera"  onclick="addChooseFile()" style="cursor: pointer;"></i>
		        			<div class="images-upload-block">
		        				<input type="file" name="image" class="image-uploader" id="hidden">
		        			</div>
		        		</div>
		        		<div class="col-sm-9" style="margin-top: 20px">
										<div class="row">
											<div class="col-md-6">
												<label> الاسم باللغه العربيه</label>
												<input type="text" name="name_ar" class="form-control" placeholder=" الاسم باللغه العربيه" style="margin-bottom: 10px">
											</div>
											<div class="col-md-6">
													<label>  الاسم باللغه الانجليزيه</label>
												<input type="text" name="name_en" class="form-control"placeholder=" الاسم باللغه الانجليزيه" style="margin-bottom: 10px">
											</div>
											<div class="col-md-6">
													<label>وقت بدا العمل</label>
													<input type="time" name="start_time" class="form-control" placeholder=" وقت بدء العمل" style="margin-bottom: 10px">
											</div>
											<div class="col-md-6">
													<label>وقت انتهاء العمل</label>
												<input type="time" name="end_time" class="form-control" placeholder=" وقت انتهاء العمل" style="margin-bottom: 10px">
											</div>
										
										
		        		</div>
	        		</div>
						</div>
					<div class="row">
						<div class="col-md-6">
							<label>وصف المطعم باللغه العربيه</label>
							<input type="text" name="desc_ar" class="form-control" placeholder=" وصف المطعم باللغه العربيه" style="margin-bottom: 10px">
						</div>
						<div class="col-md-6">
							<label>وصف المطعم باللغه الانجليزيه</label>
							<input type="text" name="desc_en" class="form-control" placeholder=" وصف المطعم باللغ الانجليزيه" style="margin-bottom: 10px">
						</div>
					</div>
					<hr>
					<div class="row">
						<div class="col-sm-3">
							<label>الاصناف المتاحه </label>
						</div>
						<div class="col-sm-9">
							@forelse ($categories as $category)
								<label>
									<span>{{$category->name_ar}}</span>
									<input type="checkbox" name="categories[]" value="{{$category->id}}" />
								</label>
							@empty
								<span>لايوجد اى صنف</span>
							@endforelse
						
						</div>
					</div>
					
					<div class="row">
						<div class="col-sm-3">
							<label>الانشطه المتاحه </label>
						</div>
						<div class="col-sm-9">
							@forelse ($activities as $activity)
								<label>
									<span>{{$activity->name_ar}}</span>
									<input type="checkbox" name="activities[]" value="{{$activity->id}}" />
								</label>
								@empty
									<span>لايوجد اى نشاط</span>
							@endforelse
						</div>
					</div>
					<div class="row">
						<div class="col-sm-3">
							<label>خدمات اضافيه المتاحه </label>
						</div>
						<div class="col-sm-9">
							@forelse ($properties as $property)
								<label>
									<span>{{$property->name_ar}}</span>
									<input type="checkbox" name="properties[]" value="{{$property->id}}" />
								</label>
							@empty
									<span>لايوجد اى خدمات</span>
							@endforelse
						</div>
					</div>
					<hr>
					<div class="row">
		        		<div class="col-sm-6">
									<label> هل يوجد خدمة التوصيل </label>
									<select name="delivery" class=" form-control delivery" id="delivery"  data-target="price-add,postion-add">
										<option value="yes">Yes</option>
										<option value="no">No</option>
									</select>
		        		</div>

		        		<div class="col-sm-6" id="price-add">
										<label> ثمن خدمة التوصيل </label>
										<input type="text" name="delivery_coast" class="form-control" placeholder=" ثمن خدمة التوصيل" style="margin-bottom: 10px">
									</div>
								<div class="col-sm-12"  id="postion-add">
										<label>اماكن التوصيل </label>
										<input type="text"  name="delivery_postion" class="form-control" placeholder="اماكن التوصيل" style="margin-bottom: 10px">
									</div>
									<div class="col-sm-12">
										<label> العنوان</label>
										<input type="text" readonly name="address" class="form-control" placeholder=" العنوان">
										<input type="hidden" id="lat" name="lat">
										<input type="hidden" id="lng" name="lng">
									</div>
										
	        		</div>
							<div id="map" style="width:100%;height:200px;margin-top:15px;">
							
							</div>
			        <div class="col-sm-12" style="margin-top: 10px">
				      	<button type="submit" class="btn btn-primary addCategory">اضافه</button>
				        <button type="button" class="btn btn-secondary" data-dismiss="modal">أغلاق</button>
			        </div>

	        	</form>
					</div>
					<div>
						
						
					</div>
	      </div>

	    </div>
	  </div>
	</div>
	<!-- /Add user Modal -->

	<!-- Edit user Modal -->
	<div class="modal fade" id="exampleModal2" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
	  <div class="modal-dialog" role="document">
	    <div class="modal-content">
	      <div class="modal-header">
	        <h5 class="modal-title" id="exampleModalLabel"> تعديل مطعم : <span class="userName"></span> </h5>
	      </div>
	      <div class="modal-body">
	        	<form action="{{route('edit-shop')}}" method="post" enctype="multipart/form-data">

	        		<!-- token and user id -->
	        		{{csrf_field()}}
	        		<input type="hidden" name="id" value="">
	        		<!-- /token and user id -->
	        		<div class="row">
		        		<div class="col-sm-3 text-center">
		        			<label >اختيار صوره</label>
		        			<img src="" class="photo" style="width: 120px;height: 120px;cursor: pointer" onclick="ChooseFile()">
		        			<input type="file" id="upload-image" name="edit_image" style="display: none;">
		        		</div>
								<div class="col-sm-9" style="margin-top: 10px">
										<div class="row">
											<div class="col-md-6">
												<label> الاسم باللغه العربيه</label>
												<input type="text" name="edit_name_ar" class="form-control">
											</div>
											<div class="col-md-6">
												<label>  الاسم باللغه الانجليزيه</label>
												<input type="text" name="edit_name_en" class="form-control">
											</div>
											<div class="col-md-6">
												<label>وقت بدا العمل</label>
												<input type="time" name="edit_start_time" class="form-control" placeholder=" الاسم الاول" style="margin-bottom: 10px">
											</div>
											<div class="col-md-6">
													<label>وقت انتهاء العمل</label>
												<input type="time" name="edit_end_time" class="form-control" placeholder="الاسم الاخير" style="margin-bottom: 10px">
											</div>
										
										</div>
									</div>
						</div>
						
						<div class="row">
							<div class="col-md-6">
								<label>وصف المطعم باللغه العربيه</label>
								<input type="text" name="edit_desc_ar" class="form-control"  placeholder=" وصف المطعم باللغه العربيه" style="margin-bottom: 10px">
							</div>
							<div class="col-md-6">
								<label>وصف المطعم باللغه الانجليزيه</label>
								<input type="text" name="edit_desc_en" class="form-control" placeholder=" وصف المطعم باللغه لانجليزيه" style="margin-bottom: 10px">
							</div>
						</div>
						<hr>
						<div class="row">
							<div class="col-sm-3">
								<label>الاصناف المتاحه </label>
							</div>
							<div class="col-sm-9 " id="cat">
								@foreach ($categories as $category)
									<label>
										<span>{{$category->name_ar}}</span>
										<input type="checkbox" name="edit_categories[]" value="{{$category->id}}" />
									</label>
								@endforeach
							</div>
						</div>
						<div class="row">
							<div class="col-sm-3">
								<label>الانشطه المتاحه </label>
							</div>
							<div class="col-sm-9" id="act">
								@foreach ($activities as $activity)
									<label>
										<span>{{$activity->name_ar}}</span>
										<input type="checkbox" name="edit_activities[]" value="{{$activity->id}}" />
									</label>
								@endforeach
							</div>
						</div>
						<div class="row">
							<div class="col-sm-3">
								<label>خدمات اضافيه المتاحه </label>
							</div>
							<div class="col-sm-9" id="prop">
								@forelse ($properties as $property)
									<label>
										<span>{{$property->name_ar}}</span>
										<input type="checkbox" name="edit_properties[]" value="{{$property->id}}" />
									</label>
								@empty
										<span>لايوجد اى خدمات</span>
								@endforelse
							</div>
						</div>
						<hr>
	        	<div class="row ">
		        		<div class="col-sm-6"  style="margin-top:10px;">
										<label> هل يوجد خدمة التوصيل </label>
										<select name="delivery" class=" form-control delivery"  data-target="coast-edit,postion-edit">
											<option value="yes">Yes</option>
											<option value="no">No</option>
										</select>
									</div>
	
									<div class="col-sm-6" id ="coast-edit">
											<label> ثمن خدمة التوصيل </label>
											<input type="text" name="edit_delivery_coast" class="form-control" placeholder=" ثمن خدمة التوصيل" style="margin-bottom: 10px">
									</div>
									<div class="col-sm-12"  id="postion-edit">
											<label>اماكن التوصيل </label>
											<input type="text"  name="edit_delivery_postion" class="form-control" placeholder="اماكن التوصيل" style="margin-bottom: 10px">
									</div>
									<div class="col-sm-12">
										<label> العنوان</label>
										<input type="text" readonly name="edit_address" class="form-control" placeholder=" العنوان">
										<input type="hidden" id="edit_lat" name="edit_lat">
										<input type="hidden" id="edit_lng" name="edit_lng">
									</div>

									<div id="map1" style="width:100%;height:200px;margin-top:10px;">
							
									</div>
									<div class="col-sm-12" style="margin-top: 10px">
											<button type="submit" class="btn btn-primary" >حفظ التعديلات</button>
											<button type="button" class="btn btn-secondary" data-dismiss="modal">أغلاق</button>
									</div>
								</div>
						</div>
			      </div>
	        	</form>
	      </div>
	    </div>
	  </div>
	</div>
	<!-- /Edit user Modal -->



</div>

<!-- javascript -->
@section('script')
<script type="text/javascript" src="{{asset('dashboard/js/plugins/tables/datatables/datatables.min.js')}}"></script>
<script type="text/javascript" src="{{asset('dashboard/js/plugins/forms/selects/select2.min.js')}}"></script>
<script type="text/javascript" src="{{asset('dashboard/js/pages/datatables_basic.js')}}"></script>
@endsection

{{-- add script  to show coast and other in form --}}
<script type="text/javascript">
	$(".delivery").change(function(){
		// alert("SDA")
		let targe = $(this).data("target").split(",")
		if($(this).val() == "yes")
		{
			console.log($("#"+targe[0]))
			$("#"+targe[0]+" input").removeAttr("readonly")
			$("#"+targe[1]).fadeIn()
		}
		else
		{	
			$("#"+targe[0]+" input").attr("readonly","true");
			$("#"+targe[1]).fadeOut()
			$("#"+targe[1]+" input");
		}
	})
</script>
{{-- edit  --}}

<script type="text/javascript">

	$('.openEditmodal').on('click',function(){
		//get valus 
		var id        			 = $(this).data('id')
		var name_ar       	 = $(this).data('name_ar')
		var image     			 = $(this).data('photo')
		var name_en    	  	 = $(this).data('name_en')
		var delivery  	  	 = $(this).data('delivery')
		var delivery_postion = $(this).data('delivery_postion')
		var start_time    	 = $(this).data('start_time')
		var end_time   			 = $(this).data("end_time")
		var delivery_coast 	 = $(this).data("delivery_coast")
		var lat        			 = $(this).data("lat")
		var lng 						 = $(this).data("lng")
		var desc_ar 				 = $(this).data("desc_ar")
		var desc_en			   	 = $(this).data("desc_en")
		var categories       = $(this).data("categories")
		var activities       = $(this).data("activities")
		var properties		   = $(this).data("properties")
		
		
		//set values in modal inputs
	
		$("input[name='id']")             	 				.val(id)
		$("input[name='edit_name_ar']")      				.val(name_ar)
		$("input[name='edit_name_en']")      				.val(name_en)
		$("input[name='edit_delivery']")    			  .val(delivery)
		$("input[name='edit_start_time']")     			.val(start_time)
		$("input[name='edit_lng']")       					.val(lng)
		$("input[name='edit_lat']")       					.val(lat)
		$("input[name='edit_end_time']")      			.val(end_time)
		$("input[name='edit_delivery_coast']")   		.val(delivery_coast)
		$("input[name='edit_delivery_postion']")    .val(delivery_postion)
		$("input[name='edit_desc_ar']")              .val(desc_ar)
		$("input[name='edit_desc_en']")    				  .val(desc_en)
		var link = "{{asset('dashboard/uploads/shops/')}}" +'/'+ image
		$(".photo").attr('src',link)
		// category
		if(categories.length > 0)
		{
			$("#cat input[type='checkbox']").each(function(){
				if(categories.find( cat => cat.id == $(this).val()))
				{
					$(this).attr('checked','')
				}
			});
		}
		// activity
		if(activities.length > 0)
		{
			$("#act input[type='checkbox']").each(function(){
				if(activities.find( cat => cat.id == $(this).val()))
				{
					$(this).attr('checked','')
				}
			});
		}
		// props
		if(properties.length > 0)
		{
			$("#prop input[type='checkbox']").each(function(){
				if(properties.find( cat => cat.property_id == $(this).val()))
				{
					$(this).attr('checked','')
				}
			});
		}
		// delivey
		$('#delivery option').each(function(){
			if($(this).val() == delivery)
			{
				$(this).attr('selected','')
			}
		});
		mapSetting(lat ,lng)

	})

	


</script>

<!-- other code -->
<script type="text/javascript">

	function ChooseFile(){
		// alert("jal")
		$("input[name='edit_image']").click()
		}
	function addChooseFile(){$("input[name='image']").click()}

	//stay in current tab after reload
	$(function() { 
	    // for bootstrap 3 use 'shown.bs.tab', for bootstrap 2 use 'shown' in the next line
	    $('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
	        // save the latest tab; use cookies if you like 'em better:
	        localStorage.setItem('lastTab', $(this).attr('href'));
	    });

	    // go to the latest tab, if it exists:
	    var lastTab = localStorage.getItem('lastTab');
	    if (lastTab) {
	        $('[href="' + lastTab + '"]').tab('show');
	    }
	});

</script>
<!-- /other code -->

{{-- code for map  --}}

<script type="text/javascript">

	function initMap() {
			var latlng = new google.maps.LatLng('{{Auth::User()['lat']}}', '{{Auth::User()['lng']}}');
			var map = new google.maps.Map(document.getElementById('map'), {
					center: latlng,
					zoom: 15,
					disableDefaultUI: true,
					mapTypeId: google.maps.MapTypeId.ROADMAP
			});
			var marker = new google.maps.Marker({
					position: latlng,
					animation: google.maps.Animation.DROP,
					map: map,
					draggable: true
			});
			google.maps.event.addListener(marker, 'dragend', function (event) {
					document.getElementById("lat").value = this.getPosition().lat();
					document.getElementById("lng").value = this.getPosition().lng();

					var latitude = roundOf(this.getPosition().lat(), 4);
					var longitude = roundOf(this.getPosition().lng(), 4);

					function roundOf(n, p) {
							const n1 = n * Math.pow(10, p + 1);
							const n2 = Math.floor(n1 / 10);
							if (n1 >= (n2 * 10 + 5)) {
									return (n2 + 1) / Math.pow(10, p);
							}
							return n2 / Math.pow(10, p);
					}

					var urlTo = "https://maps.googleapis.com/maps/api/geocode/json?latlng=" + latitude + "," + longitude + "&key=AIzaSyC5uC_mExFIMSehvCgsjegxcF7mTpKmI4w&sensor=true&language=ar";
					$.ajax({
							url: [urlTo],
							cache: false,
							success: function (data) {
									var address = data.results[0].formatted_address;
									console.log(latitude)
									$("input[name='lat']").val(latitude);
									$("input[name='lng']").val(longitude);
									$("input[name='address']").val(address);	
							}
					});
			});
	}

</script>
<!-- edit map setting  !-->
<script type="text/javascript">
	function mapSetting(lat ,lng){
		var urlTo = "https://maps.googleapis.com/maps/api/geocode/json?latlng="+lat+","+lng+"&key=AIzaSyC5uC_mExFIMSehvCgsjegxcF7mTpKmI4w&sensor=true&language=ar";
					$.ajax({
							url:[urlTo] ,
							cache: false,
							success: function(data){
									var ar = data.results[0].formatted_address;
									$("input[name='edit_address']") .val(ar);
							}
					});
			var latlng = new google.maps.LatLng(lat , lng);
			var map = new google.maps.Map(document.getElementById('map1'), {
					center: latlng,
					zoom: 15,
					disableDefaultUI: true,
					zoomControl: true,
					mapTypeId: google.maps.MapTypeId.ROADMAP,

			});
			var marker = new google.maps.Marker({
					position: latlng,
					map: map,
					animation: google.maps.Animation.DROP,
					draggable: true
			});
			google.maps.event.addListener(marker, 'dragend', function (event) {
					document.getElementById("edit_lat").value  = this.getPosition().lat();
					document.getElementById("edit_lng").value  = this.getPosition().lng();

					var latitude  =  roundOf(this.getPosition().lat(),4);
					var longitude =  roundOf(this.getPosition().lng(),4);
				
					function roundOf(n, p) {
							const n1 = n * Math.pow(10, p + 1);
							const n2 = Math.floor(n1 / 10);
							if (n1 >= (n2 * 10 + 5)) {
									return (n2 + 1) / Math.pow(10, p);
							}
							return n2 / Math.pow(10, p);
					}
					var urlTo = "https://maps.googleapis.com/maps/api/geocode/json?latlng="+latitude+","+longitude+"&key=AIzaSyC5uC_mExFIMSehvCgsjegxcF7mTpKmI4w&sensor=true&language=ar";
					$.ajax({
							url:[urlTo] ,
							cache: false,
							success: function(data){
									var ar = data.results[0].formatted_address;
									$("input[name='edit_lat']").val(latitude);
									$("input[name='edit_lng']").val(longitude);
									$("input[name='edit_address']") .val(ar);
							}
					});
			});
	}
</script>


<script async defer
			src="https://maps.googleapis.com/maps/api/js?key=AIzaSyC5uC_mExFIMSehvCgsjegxcF7mTpKmI4w&callback=initMap">
</script>
 {{-- end --}}

@endsection