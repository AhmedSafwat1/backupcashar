@extends('dashboard.layout.master')

<!-- style -->
@section('style')
    <style type="text/css">
        .modal .icon-camera
        {
            font-size: 100px;
            color: #797979
        }

        .modal input
        {
            margin-bottom: 4px
        }

        .reset
        {
            border:none;
            background: #fff;
            margin-right: 11px;
        }

        .icon-trash
        {
            margin-left: 8px;
            color: red;
        }

        .dropdown-menu
        {
            min-width: 88px;
        }

        #hidden
        {
            display: none;
        }
    </style>
@endsection
<!-- /style -->
@section('content')
    <div class="panel panel-flat">
        <div class="panel-heading">
            <h5 class="panel-title">قائمة الاصناف</h5>
            <div class="heading-elements">
                <ul class="icons-list">
                    <li><a data-action="collapse"></a></li>
                    <li><a data-action="reload"></a></li>
                    <!-- <li><a data-action="close"></a></li> -->
                </ul>
            </div>
        </div>

        <!-- buttons -->
        <div class="panel-body">
            <div class="row">
                <div class="col-xs-4">
                    <button class="btn bg-blue btn-block btn-float btn-float-lg openAddModal" type="button" data-toggle="modal" data-target="#exampleModal"><i class="icon-plus3"></i> <span>اضافة صنف</span></button>
                </div>
                <div class="col-xs-4">
                    <button class="btn bg-purple-300 btn-block btn-float btn-float-lg" type="button"><i class="icon-list-numbered"></i> <span>عدد الاصناف : {{count($categories)}} </span> </button></div>
                <div class="col-xs-4">
                    <a href="{{route('logout')}}" class="btn bg-warning-400 btn-block btn-float btn-float-lg" type="button"><i class="icon-switch"></i> <span>خروج</span></a>
                </div>
            </div>
        </div>
        <!-- /buttons -->

        <table class="table datatable-basic">
            <thead>
            <tr>
                <th>صورة صنف</th>
                <th>الصنف باللغه العربيه</th>
                <th> الصنف باللغه الانجليزيه</th>
                <th>تاريخ الاضافه</th>
            
                <th>التحكم</th>
            </tr>
            </thead>
            <tbody>
            @foreach($categories as $u)
                <tr>
                    <td><img src="{{asset('dashboard/uploads/categories/'.$u->icon)}}" style="width:40px;height: 40px" class="img-circle" alt=""></td>
                    <td>{{$u->name_ar}}</td>
                    <td>{{$u->name_en}}</td>
                   
                    <td>{{$u->created_at->diffForHumans()}}</td>
                    
                    
                    <td>
                        <ul class="icons-list">
                            <li class="dropdown">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                    <i class="icon-menu9"></i>
                                </a>

                                <ul class="dropdown-menu dropdown-menu-right">
                                    <!-- edit button -->
                                    <li>
                                        <a href="#" data-toggle="modal" data-target="#exampleModal2" class="openEditmodal"
                                           data-id="{{$u->id}}"
                                           data-icon="{{$u->icon}}"
                                           data-name_ar="{{$u->name_ar}}"
                                           data-name_en="{{$u->name_en}}"
                                           >
                                            <i class="icon-pencil7"></i>تعديل
                                        </a>
                                    </li>                                
                                    <!-- delete button -->
                                    <form action="{{route('delete-category')}}" method="POST">
                                        {{csrf_field()}}
                                        <input type="hidden" name="id" value="{{$u->id}}">
                                        <li><button type="submit" class="generalDelete reset"><i class="icon-trash"></i>حذف</button></li>
                                    </form>
                                </ul>
                            </li>
                        </ul>
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>

        <!-- Add category Modal -->
        <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">أضافة صنف جديد</h5>
                    </div>
                    <div class="modal-body">
                        <div class="row">
                            <form action="{{route('add-category')}}" method="POST" enctype="multipart/form-data">
                                {{csrf_field()}}

                                <div class="row ">
                                <div class="col-sm-3 text-center">
                                        <label style="margin-bottom: 0">اختيار صورة الصنف</label>
                                        <i class="icon-camera"  onclick="addChooseFile()" style="cursor: pointer;"></i>
                                        <div class="images-upload-block">
                                            <input type="file" name="icon" class="image-uploader" id="hidden">
                                        </div>
                                    </div>
                                    <div class="col-sm-9 " style="margin-top: 20px">
                                        <input type="text" name="name_ar" class="form-control" placeholder="  اسم  الصنف باللغه العربيه " style="margin-bottom: 10px">
                                        <input type="text" name="name_en" class="form-control" placeholder="  اسم الصنف باللغه الانجليزيه ">
                                        
                                    </div>
                                </div>

                                

                                <div class="col-sm-12" style="margin-top: 10px">
                                    <button type="submit" class="btn btn-primary ">اضافه</button>
                                    <button type="button" class="btn btn-secondary" data-dismiss="modal">أغلاق</button>
                                </div>

                            </form>
                        </div>
                    </div>

                </div>
            </div>
        </div>
        <!-- /Add category Modal -->

        <!-- Edit category Modal -->
        <div class="modal fade" id="exampleModal2" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel"> تعديل صنف : <span class="userName"></span> </h5>
                    </div>
                    <div class="modal-body">
                        <form action="{{route('edit-category')}}" method="post" enctype="multipart/form-data">

                            <!-- token and user id -->
                            {{csrf_field()}}
                            <input type="hidden" name="id" value="">
                            <!-- /token and user id -->
                            <div class="row">
                                <div class="col-sm-3 text-center">
                                    <label style="margin-bottom: 0">اختيار صورة  الصنف</label>
                                    <img src="" class="photo" style="width: 120px;height: 120px;cursor: pointer" onclick="ChooseFile()">
                                    <div class="images-upload-block">
                                        <input type="file" name="edit_icon" class="image-uploader" id="hidden">
                                    </div>
                                </div>
                                <div class="col-sm-9" style="margin-top: 10px">
                                    <label>الاسم باللغه العربيه </label>
                                    <input type="text" name="edit_name_ar" class="form-control">
                                    <label>الاسم باللغه الانجليزيه </label>
                                    <input type="text" name="edit_name_en" class="form-control">
                                    
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-12" style="margin-top: 10px">
                                    <button type="submit" class="btn btn-primary" >حفظ التعديلات</button>
                                    <button type="button" class="btn btn-secondary" data-dismiss="modal">أغلاق</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <!-- /Edit category Modal -->

        

    </div>

    <!-- javascript -->
@section('script')
    <script type="text/javascript" src="{{asset('dashboard/js/plugins/tables/datatables/datatables.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('dashboard/js/plugins/forms/selects/select2.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('dashboard/js/pages/datatables_basic.js')}}"></script>

@endsection



<script type="text/javascript">

    $('.openEditmodal').on('click',function(){
        //get valus
        var id            = $(this).data('id')
        var name_ar       = $(this).data('name_ar')
        var name_en       = $(this).data('name_en')
        var icon          = $(this).data('icon')
        var link = "{{asset('dashboard/uploads/categories/')}}" +'/'+ icon
		$(".photo").attr('src',link)

        //set values in modal inputs
        $("input[name='id']")                  .val(id)
        $("input[name='edit_name_ar']")        .val(name_ar)
        $("input[name='edit_name_en']")        .val(name_en)
      
        


    })

   


</script>

<!-- other code -->
<script type="text/javascript">

   
    function ChooseFile(){$("input[name='edit_icon']").click()}
    function addChooseFile(){$("input[name='icon']").click()}

    //stay in current tab after reload
    $(function() {
        // for bootstrap 3 use 'shown.bs.tab', for bootstrap 2 use 'shown' in the next line
        $('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
            // save the latest tab; use cookies if you like 'em better:
            localStorage.setItem('lastTab', $(this).attr('href'));
        });

        // go to the latest tab, if it exists:
        var lastTab = localStorage.getItem('lastTab');
        if (lastTab) {
            $('[href="' + lastTab + '"]').tab('show');
        }
    });

</script>
<!-- /other code -->

@endsection