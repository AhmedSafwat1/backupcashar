@extends('dashboard.layout.master')
	
<!-- style -->
@section('style')
<style type="text/css">
	.modal .icon-camera
	{
		font-size: 100px;
		color: #797979
	}

	.modal input
	{
		margin-bottom: 4px
	}

	.reset
	{
		border:none;
		background: #fff;
		margin-right: 11px;
	}
	.mt-1
	{
		margin-top: 10px;
	}
	.icon-trash
	{
		margin-left: 8px;
		color: red;
	}

	.dropdown-menu
	{
		min-width: 88px;
	}

	#hidden
	{
		display: none;
	}
</style>
<link href="{{asset('dashboard/fileinput/css/fileinput.min.css')}}" rel="stylesheet" type="text/css">
<link href="{{asset('dashboard/fileinput/css/fileinput-rtl.min.css')}}" rel="stylesheet" type="text/css">
<link href="{{asset('dashboard/bgrins/spectrum.css')}}" rel='stylesheet' >

@endsection

<!-- /style -->
@section('content')


<div class="row">
	<div class="col-md-12">
		<div class="panel panel-flat">
			<div class="panel-heading">
			<h6 class="panel-title">
				
				<form action="{{route('shop-products')}}" method="get">
						{{-- {{csrf_field()}} --}}
					<input type="hidden" name="shop_id" value="{{$shop->id}}">
					<button type="submit" class="reset ">{{$shop->name_ar}}</button>
				</form>
			</h6>
				<div class="heading-elements">
					<ul class="icons-list">
                		<li><a data-action="reload"></a></li>
                	</ul>
            	</div>
			</div>

			<div class="panel-body">
				<div class="tabbable">
					<ul class="nav nav-tabs">
						<!-- site setting -->
                   		 <li class="active"><a href="#basic-tab1" data-toggle="tab">{{$product->name_ar}}</a></li>
						<!-- addtion media -->
						<li><a href="#basic-tab2" data-toggle="tab"> الاضافات</a></li>
						<li><a href="#basic-tab3" data-toggle="tab"> لاحجام</a></li>
					</ul>

					<div class="tab-content">

						<!-- site setting -->
						<div class="tab-pane active" id="basic-tab1">
							<div class="row">
								<!-- main setting -->
								<div class="col-md-12">
										<div class="panel panel-flat">
											<div class="panel-heading">
												<h5 class="panel-title"> تعديل المنتج </h5>
												<div class="heading-elements">
													<ul class="icons-list">
								                		<li><a data-action="collapse"></a></li>
								                		<li><a data-action="reload"></a></li>
								                	</ul>
							                	</div>
											</div>

											<div class="panel-body">
												<form action="{{route('edit-shop-product-save')}}" method="post" class="form-horizontal" enctype="multipart/form-data">
													{{csrf_field()}}
													<input type="hidden" name="shop_id"    value="{{$shop->id}}"/>
													<input type="hidden" name="product_id" value="{{$product->id}}"/>
													<div class="form-group">
														<label class="col-lg-3 control-label">صورة المنتج:</label>
														<div class="col-lg-6">
															<img src="{{asset('dashboard/uploads/products/'.$product->image)}}" title="اختيار صوره" onclick="sitelogo()" style="height: 210px; width: 210px;cursor: pointer;border-radius:100%">
															<input type="file" name="image" id="hidden">
														</div>
													</div>
													<div class="form-group">
														<label class="col-lg-3 control-label">اسم المنتج باللغه العربيه:</label>
														<div class="col-lg-9">
															<input type="text" value="{{$product->name_ar}}" name="name_ar" class="form-control" placeholder="اسم المنتج باللغه العربيه">
														</div>
													</div>
													<div class="form-group">
														<label class="col-lg-3 control-label">اسم المنتج باللغه الانجليزيه:</label>
														<div class="col-lg-9">
															<input type="text" value="{{$product->name_en}}" name="name_en" class="form-control" placeholder="اسم المنتج باللغه الانجليزي">
														</div>
													</div>
													<div class="form-group">
														<label class="col-lg-3 control-label">وصف المنتج باللغه الانجليزيه:</label>
														<div class="col-lg-9">
															<input type="text" value="{{$product->desc_ar}}" name="desc_ar" class="form-control" placeholder="وصف  المنتج باللغه العربيه">
														</div>
													</div>
													<div class="form-group">
														<label class="col-lg-3 control-label">وصف المنتج باللغه الانجليزيه:</label>
														<div class="col-lg-9">
															<input type="text" value="{{$product->desc_en}}" name="desc_en" class="form-control" placeholder="وصف  المنتج باللغه العربيه">
														</div>
													</div>
													<div class="form-group">
														<label class="col-lg-3 control-label">  وقت المطلوب للاعداد بالدقائق:</label>
														<div class="col-lg-9">
															<input type="number" value="{{$product->prepared}}" name="prepared" class="form-control" placeholder="وقت المطلوب للاعداد بالدقائق">
														</div>
													</div>
													<div class="form-group">
														<label class="col-lg-3 control-label">  صنف المنتج:</label>
														<div class="col-lg-9">
															<select name="category_id" class=" form-control delivery" id="catAjex"  >
																@foreach ($categories as $cat)
																	<option value="{{$cat->id}}"  @if($cat->id == $product->category_id ) selected @endif  >{{$cat->name_ar}}</option>
																@endforeach
														</select>
														</div>
													</div>
													<div class="text-left">
														<button type="submit" class="btn btn-primary">حفظ التعديلات</button>
													</div>
												</form>
												</div>
										</div>
								</div>
							</div>
						</div>

						<!-- addtion media -->
						<div class="tab-pane" id="basic-tab2">
								<div class="col-md-12">
									{{csrf_field()}}
									<div class="panel panel-flat">
										<div class="panel-heading">
											<h5 class="panel-title"> اضافات المنتج </h5>
											<div class="heading-elements">
												{{-- <ul class="icons-list">
													<button class="btn btn-primary" type="button" data-toggle="modal" data-target="#exampleModal"><i class="icon-plus3"></i> اضافة  </button>
												</ul> --}}
											</div>
										</div>

										<div class="panel-body">
												<!-- ============== -->
	
												
												<table class="table datatable-basic ">
														<thead>
														<tr>
															<th>الاضافه باللغه العربيه</th>
															<th> الاضافه باللغه الانجليزيه</th>
															<th>السعر</th>
															<th>الكميه</th>
															<th>ملاحظه</th>
															<th>التحكم</th>
														</tr>
														</thead>
														<tbody>
														@foreach($addtions as $u)
															<tr>
															
																<td>{{$u->Addtion->name_ar}}</td>
																<td>{{$u->Addtion->name_en}}</td>
																<td>{{$u->price}} ريال</td>
																<td>{{$u->quantity}}</td>
																<td>{{$u->note}}</td>
																<td>
																	<ul class="icons-list">
																		<li class="dropdown">
																			<a href="#" class="dropdown-toggle" data-toggle="dropdown">
																				<i class="icon-menu9"></i>
																			</a>
											
																			<ul class="dropdown-menu dropdown-menu-right">
																				<!-- edit button -->
																				<li>
																					<a href="#" data-toggle="modal" data-target="#exampleModal2" class="openEditmodal open"
																					data-id		    ="{{$u->id}}"
																					data-addtion_id ="{{$u->Addtion->id}}"
																					data-note		="{{$u->note}}"
																					data-price		="{{$u->price}}"
																					data-quantity	="{{$u->quantity}}"
																					data-addtions   ="{{$product->Category->addtions}}"
																					data-Hasaddtions="{{$product->ProductAddtions}}"
																					data-adnow 		="{{$u->Addtion}}"
																					>
																						<i class="icon-pencil7"></i>تعديل
																					</a>
																				</li>                                
																				<!-- delete button -->
																				<form action="{{route('delete-addtion-product')}}" method="POST">
																					{{ csrf_field()}}
																					<input type="hidden" name="product_id" value="{{$product->id}}">
																					<input type="hidden" name="shop_id" value="{{$shop->id}}">
																					<input type="hidden" name="id" value="{{$u->id}}">
																					<li><button type="submit" class="generalDelete reset"><i class="icon-trash"></i>حذف</button></li>
																				</form>
																			</ul>
																		</li>
																	</ul>
																</td>
															</tr>
														@endforeach
														</tbody>
													</table>
																						
	
	
	
												<!-- ============== -->
	
											</div>
									</div>
								</div>
						</div>
						<!-- size media -->
						<div class="tab-pane" id="basic-tab3">
								<div class="col-md-12">
									{{csrf_field()}}
									<div class="panel panel-flat">
										<div class="panel-heading">
											<h5 class="panel-title"> احجام  المنتج </h5>
											<div class="heading-elements">
												{{-- <ul class="icons-list">
													<button class="btn btn-primary" type="button" data-toggle="modal" data-target="#exampleModal"><i class="icon-plus3"></i> اضافة  </button>
												</ul> --}}
											</div>
										</div>

										<div class="panel-body">
												<!-- ============== -->
	
												
												<table class="table datatable-basic ">
														<thead>
														<tr>
															<th>الحجم</th>
															<th>السعر</th>
															<th>الكميه</th>
															<th>تاريخ الاضافه</th>
															<th>التحكم</th>
														</tr>
														</thead>
														<tbody>
														@foreach($product->Sizes as $u)
															<tr>
															
																<td>{{$u->size}}</td>
																<td>{{$u->price}} ريال</td>
																<td>{{$u->quantity}}</td>
																<td>{{$u->created_at->diffForHumans()}}</td>
																<td>
																	<ul class="icons-list">
																		<li class="dropdown">
																			<a href="#" class="dropdown-toggle" data-toggle="dropdown">
																				<i class="icon-menu9"></i>
																			</a>
											
																			<ul class="dropdown-menu dropdown-menu-right">
																				<!-- edit button -->
																				<li>
																					<a href="#" data-toggle="modal" data-target="#exampleModal4" class="openEditmodal4"
																					data-id="{{$u->id}}"
																					data-size="{{$u}}"
																					data-price="{{$u->price}}"
																					data-quantity="{{$u->quantity}}"
																					>
																						<i class="icon-pencil7"></i>تعديل
																					</a>
																				</li>                                
																				<!-- delete button -->
																				<form action="{{route('delete-size-product')}}" method="POST">
																					{{csrf_field()}}
																					<input type="hidden" name="product_id" value="{{$product->id}}">
																					<input type="hidden" name="shop_id" value="{{$shop->id}}">
																					<input type="hidden" name="id" value="{{$u->id}}">
																					<li><button type="submit" class="generalDelete reset"><i class="icon-trash"></i>حذف</button></li>
																				</form>
																			</ul>
																		</li>
																	</ul>
																</td>
															</tr>
														@endforeach
														</tbody>
													</table>
																						
	
	
	
												<!-- ============== -->
	
											</div>
									</div>
								</div>
						</div>
{{-- ================= --}}
					</div>
				</div>
			</div>
		</div>



		




	</div>



	<!-- ========================================================= -->
	<!-- Edit addtion Modal -->
	<div class="modal fade" id="exampleModal2" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel"> تعديل اضافه : <span class="userName"></span> </h5>
                    </div>
                    <div class="modal-body">
                        <form action="{{route('edit-addtion-product')}}" method="post" enctype="multipart/form-data">

                            <!-- token and user id -->
							{{csrf_field()}}
							<input type="hidden" name="id" value="">
							<input type="hidden" name="edit_product_id" value="">
							<input type="hidden" name="edit_shop_id" value="">
							<!-- /token and user id -->
							<div class="row">
									<div class="row">
										<div class="col-sm-3 mt-1">
											<label>الاضافه</label>
										</div>
										<div class="col-sm-9 .mt-1">
											<span id="adnow"></span>
											<select name="edit_addtion_id" class=" form-control" id="edit_addtion">
												
											</select>
											<p class='mt-1 messg ' styule="display:none">لايوجد اضافات متوفره</p>
										</div>
									</div>
									<div class="row">
										<div class="col-sm-3 mt-1">
											<label>الكميه</label>
										</div>
										<div class="col-sm-9 mt-1">
											<input type="number" name="edit_quantity" class="form-control">
										</div>
									</div>
									<div class="row">
										<div class="col-sm-3 mt-1">
											<label>السعر</label>
										</div>
										<div class="col-sm-9 mt-1">
											<input type="number" name="edit_price" class="form-control">
										</div>
									</div>
									<div class="row">
										<div class="col-sm-3 mt-1">
											<label> ملاحظات</label>
										</div>
										<div class="col-sm-9 mt-1">
											<input type="text" name="edit_note" class="form-control">
										</div>
									</div>
							</div>
                            <div class="row">
                                <div class="col-sm-12" style="margin-top: 10px">
                                    <button type="submit" class="btn btn-primary" >حفظ التعديلات</button>
                                    <button type="button" class="btn btn-secondary" data-dismiss="modal">أغلاق</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
		<!-- /Edit city Modal -->
		 <!-- Edit sizes Modal -->
			<div class="modal fade" id="exampleModal4" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
					<div class="modal-dialog" role="document">
							<div class="modal-content">
									<div class="modal-header">	
											<h5 class="modal-title" id="exampleModalLabel"> تعديل  حجم للمنتج : <span class="userName"></span> </h5>
									</div>
									<div class="modal-body">
											<form action="{{route('edit-size-product')}}" method="post" enctype="multipart/form-data">
				
													<!-- token and user id -->
													{{csrf_field()}}
													<input type="hidden" name="id" value="">
													<input type="hidden" name="size_product_id" value="">
													<input type="hidden" name="size_shop_id" value="">
													<!-- /token and user id -->
													<div class="row">
															<div class="row">
																<div class="col-sm-3 mt-1">
																	<label>الحجم</label>
																</div>
																<div class="col-sm-9 .mt-1">
																	<input type="text" name="size" placeholder="الحجم" class="form-control">
																</div>
															</div>
															<div class="row">
																<div class="col-sm-3 mt-1">
																	<label>الكميه</label>
																</div>
																<div class="col-sm-9 mt-1">
																	<input type="number" name="quantity" placeholder=" الكميه" class="form-control">
																</div>
															</div>
															<div class="row">
																<div class="col-sm-3 mt-1">
																	<label> السعر</label>
																</div>
																<div class="col-sm-9 mt-1">
																	<input type="number" name="price"  placeholder=" السعر" class="form-control">
																</div>
															</div>
													</div>
													<div class="row">
															<div class="col-sm-12" style="margin-top: 10px">
																	<button type="submit" class="btn btn-primary" >حفظ التعديلات</button>
																	<button type="button" class="btn btn-secondary" data-dismiss="modal">أغلاق</button>
															</div>
													</div>
													
											</form>
											
									</div>
							</div>
					</div>
				</div>
				<!-- /Edit city Modal -->
</div>


<!-- javascript -->
@section('script')
<script src="{{asset('dashboard/bgrins/spectrum.js')}}"></script>
<script type="text/javascript" src="{{asset('dashboard/js/plugins/tables/datatables/datatables.min.js')}}"></script>
<script type="text/javascript" src="{{asset('dashboard/js/plugins/forms/selects/select2.min.js')}}"></script>
<script type="text/javascript" src="{{asset('dashboard/js/pages/datatables_basic.js')}}"></script>
<script type="text/javascript">

	
	
	//select logo
	
	function sitelogo(){$("input[name='image']").click()}

	//stay in current tab after reload
	$(function() { 
	    // for bootstrap 3 use 'shown.bs.tab', for bootstrap 2 use 'shown' in the next line
	    $('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
	        // save the latest tab; use cookies if you like 'em better:
	        localStorage.setItem('lastTab', $(this).attr('href'));
	    });

	    // go to the latest tab, if it exists:
	    var lastTab = localStorage.getItem('lastTab');
	    if (lastTab) {
	        $('[href="' + lastTab + '"]').tab('show');
	    }
	});


</script>
{{-- edit --}}
<script>
	
</script>

<script type="text/javascript" src="{{asset('dashboard/fileinput/js/fileinput.min.js')}}"></script>

@endsection
<!-- /javascript -->
<script>
	$('.openEditmodal').on('click',function(){
        //get valus
        var id             = $(this).data('id')
        var addtion_id     = $(this).data('addtion_id')
        var quantity       = $(this).data('quantity')
        var note           = $(this).data('note')
		var price           = $(this).data('price')
		var shop_id        = {{$shop->id}} ;
		var product_id     = {{$product->id}} 
		var addtions       = $(this).data('addtions')
	    var Hasaddtions    = $(this).data('hasaddtions')
		var adnow		   = $(this).data('adnow')
		console.log(addtions, Hasaddtions)
        //set values in modal inputs
        $("input[name='id']")                  .val(id)
        $("input[name='edit_addtion_id']")     .val(addtion_id)
        $("input[name='edit_quantity']")       .val(quantity)
        $("input[name='edit_note']")           .val(note)
		$("input[name='edit_price']")          .val(price)
		$("input[name='edit_shop_id']")        .val(shop_id)
		$("input[name='edit_product_id']")     .val(product_id)
		$("#edit_addtion").html(" ")
		var flag2 = true;
				//set values in modal inputs
				console.log(addtions);
			$("input[name='add_shop_id']")           .val(shop_id)
			$("input[name='add_product_id']")         .val(product_id)
			$("#edit_addtion").fadeOut()
			if(addtions && addtions.length > 0)
			{
				for (const addtion of addtions) {
					let findAddtion = Hasaddtions.find( cat => cat.addtion_id == addtion.id);
					if(!findAddtion )
					{
						let option = `<option value="${addtion.id}">${addtion.name_ar}</option>`
						$("#edit_addtion").append(option);
						flag2 = false;
					}	
				}
				if(adnow)
				{
					let option = `<option value="${adnow.id} " selected>${adnow.name_ar} </option>`
					$("#edit_addtion").append(option);
					flag2 = false;
				}
				if(flag2)
				{
					$("#edit_addtion").fadeOut()
					$("#edit_addtion").next("p").fadeIn();
				}
				else
				{
					$("#edit_addtion").fadeIn()
					$("#edit_addtion").next("p").fadeOut();
				}
			}
			else
			{	
				$("#edit_addtion").fadeOut()
				$("#edit_addtion").next("p").fadeIn();
				
			}
				
	})
	$('.openEditmodal4').on('click',function(){
			var shop_id              = {{$shop->id}}
			var product_id      	 = {{$product->id}}
			var size			     = $(this).data('size')
			
			$("input[name='id']")                  	  .val(size.id)
			$("input[name='size_shop_id']")           .val(shop_id)
			$("input[name='size_product_id']")        .val(product_id)
			$("input[name='price']")                  .val(size.price)
			$("input[name='size']")                   .val(size.size)
			$("input[name='quantity']")               .val(size.quantity)

			
	}
	)
</script>
@endsection