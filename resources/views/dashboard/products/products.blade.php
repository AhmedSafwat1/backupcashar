
@extends('dashboard.layout.master')
	
	<!-- style -->
	@section('style')
		<style type="text/css">
			.modal .icon-camera
			{
				font-size: 100px;
				color: #797979
			}

			.modal input
			{
				margin-bottom: 4px
			}

			.reset
			{
				border:none;
				background: #fff;
    			margin-right: 11px;
			}

			.icon-trash
			{
				margin-left: 8px;
    			color: red;
			}

			.dropdown-menu
			{
				min-width: 88px;
			}

			#hidden
			{
				display: none;
			}
			.dropdown-menu {
						min-width: 138px;
				}
				.mt-1
				{
					margin-top: 10px;
				}

		</style>
	@endsection
	<!-- /style -->
@section('content')
<div class="panel panel-flat">
	<div class="panel-heading">
    <h5 class="panel-title">قائمة المنتجاتات ل{{$shop->name_ar}}</h5>
		<div class="heading-elements">
			<ul class="icons-list">
        		<li><a data-action="collapse"></a></li>
        		<li><a data-action="reload"></a></li>
        		<!-- <li><a data-action="close"></a></li> -->
        	</ul>
    	</div>
	</div>

	<!-- buttons -->
	<div class="panel-body">
		<div class="row">
			<div class="col-xs-4">
				<button class="btn bg-blue btn-block btn-float btn-float-lg openAddModal" type="button" data-toggle="modal" data-target="#exampleModal"><i class="icon-plus3"></i> <span>اضافة منتج</span></button>
			</div>
			<div class="col-xs-4">
				<button class="btn bg-purple-300 btn-block btn-float btn-float-lg" type="button"><i class="icon-list-numbered"></i> <span>عدد المنتجات : {{count($products)}} </span> </button>
			</div>
			<div class="col-xs-4	">
				<a href="{{route('logout')}}" class="btn bg-warning-400 btn-block btn-float btn-float-lg" type="button"><i class="icon-switch"></i> <span>خروج</span></a>
			</div>
		</div>
	</div>
	<!-- /buttons -->

	<table class="table datatable-basic">
		<thead>
			<tr>
				<th>الصوره</th>
				<th>الاسم باللغه العربيه</th>
				<th>الاسم باللغه الانجليزيه</th>
				<th>وقت الاعداد</th>
				<th>الصنف</th>
				<th>تاريخ الاضافه</th>
				<th>التحكم</th>
			</tr>
		</thead>
		<tbody>
			@foreach($products as $u)
				<tr>
					<td><img src="{{asset('dashboard/uploads/products/'.$u->image)}}" style="width:40px;height: 40px" class="img-circle" alt=""></td>
					<td>{{$u->name_ar}}</td>
					<td>{{$u->name_en}}</td>
					<td>{{$u->prepared}}</td>
					<td>{{$u->Category->name_ar}} </td>
					<td>{{$u->created_at->diffForHumans()}}</td>
					<td>
					<ul class="icons-list">
						<li class="dropdown">
							<a href="#" class="dropdown-toggle" data-toggle="dropdown">
								<i class="icon-menu9"></i>
							</a>

							<ul class="dropdown-menu dropdown-menu-right">
								<!-- edit button -->
								<li>
									<!-- edit button -->
									<form action="{{route('edit-shop-product')}}" method="get" class="mt-1">
										{{-- {{csrf_field()}} --}}
										<input type="hidden" name="shop_id" value="{{$shop->id}}">
										<input type="hidden" name="product_id" value="{{$u->id}}">
										<li><button type="submit" class=" reset"><i class="icon-pencil7"></i>تعديل</button></li>
									</form>
								</li>

								<!-- add addtions -->
							
								<li>
									<a href="#" data-toggle="modal" data-target="#exampleModal2" class="openEditmodal"
										 data-shop_id   ="{{$shop->id}}"
										 data-product_id="{{$u->id}}"
										 data-name_ar="{{$u->name_ar}}"
										 data-name_en="{{$u->name_en}}"
										 data-addtions="{{$u->Category->addtions}}"
										 data-Hasaddtions="{{$u->ProductAddtions}}"
										 >
											<i class="icon-pencil7"></i>اضافه للمنتج 
									</a>
							</li>     
								<!-- add sizes -->
							
								<li>
									<a href="#" data-toggle="modal" data-target="#exampleModal3" class="openEditmodal2"
										 data-shop_id   ="{{$shop->id}}"
										 data-product_id="{{$u->id}}"
										 data-name_ar="{{$u->name_ar}}"
										 data-name_en="{{$u->name_en}}"
										 data-sizes="{{$u->Sizes}}"
										 >
											<i class="icon-pencil7"></i>اضافه حجم 
									</a>
							</li>     
																					
								<li>																																																				
									<!-- delete button -->
									<form action="{{route('delete-product')}}" method="POST" class="mt-1">
											{{csrf_field()}}
											<input type="hidden" name="product_id" value="{{$u->id}}">
											<input type="hidden" name="shop_id" value="{{$shop->id}}">
											<li><button type="submit" class="generalDelete reset"><i class="icon-trash"></i>حذف</button></li>
									</form>
								</li>
								
							</ul>
						</li>
					</ul>
					</td>
				</tr>
			@endforeach
		</tbody>
	</table>

	<!-- Add user Modal -->
	<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
	  <div class="modal-dialog" role="document">
	    <div class="modal-content">
	      <div class="modal-header">
	        <h5 class="modal-title" id="exampleModalLabel">أضافة منتج جديد</h5>
	      </div>
	      <div class="modal-body">
	        <div class="row">
	        	<form action="{{route('add-shop-product')}}" method="POST" enctype="multipart/form-data">
	        		{{csrf_field()}}
							<input type="hidden" name="shop_id" value="{{$shop->id}}"/>
	        		<div class="row">
		        		<div class="col-sm-3 text-center">
		        			<label style="margin-bottom: 0">اختيار صوره</label>
		        			<i class="icon-camera"  onclick="addChooseFile()" style="cursor: pointer;"></i>
		        			<div class="images-upload-block">
		        				<input type="file" name="image" class="image-uploader" id="hidden">
		        			</div>
		        		</div> <!-- end images -->
		        		<div class="col-sm-9" style="margin-top: 20px">
                          <div class="row">
                                <div class="col-md-6">
                                    <label> الاسم باللغه العربيه</label>
                                    <input type="text" name="name_ar" class="form-control" placeholder=" الاسم باللغه العربيه" style="margin-bottom: 10px">
                                </div>
                                <div class="col-md-6">
                                        <label>  الاسم باللغه الانجليزيه</label>
                                    <input type="text" name="name_en" class="form-control"placeholder=" الاسم باللغه الانجليزيه" style="margin-bottom: 10px">
                                </div>
                                <div class="col-md-6">
                                        <label> وقت المطلوب للاعداد بالدقائق </label>
                                        <input type="number" name="prepared" class="form-control" placeholder=" وقت بدء العمل" style="margin-bottom: 10px">
                                </div>
                                <div class="col-md-6">
                                        <label>صنف المنتج</label>
                                        <select name="category_id" class=" form-control delivery" id="catAjex"  >
                                                @foreach ($categories as $cat)
                                                    <option value="{{$cat->id}}">{{$cat->name_ar}}</option>
                                                @endforeach
                                        </select>
                                </div>
										
										
                           </div> <!-- end eow first -->
                        
	        		    </div><!-- end col-sm-9 first -->
					</div><!-- end row first -->
					<div class="row">
						<div class="col-md-6">
							<label>وصف المنتجات باللغه العربيه</label>
							<input type="text" name="desc_ar" class="form-control" placeholder=" وصف المنتجات باللغه العربيه" style="margin-bottom: 10px">
						</div>
						<div class="col-md-6">
							<label>وصف المنتجات باللغه الانجليزيه</label>
							<input type="text" name="desc_en" class="form-control" placeholder=" وصف المنتجات باللغ الانجليزيه" style="margin-bottom: 10px">
						</div>
					</div><!-- end row  -->

			        <div class="col-sm-12" style="margin-top: 10px">
				      	<button type="submit" class="btn btn-primary addCategory">اضافه</button>
				        <button type="button" class="btn btn-secondary" data-dismiss="modal">أغلاق</button>
			        </div>

                </form><!-- end row first -->
                
			</div><!-- end row first -->
		  <div><!-- end model body first -->
						
						
		</div><!-- end mode content  -->
	  </div><!-- end model diallog -->
	</div>
  </div>
	</div> <!-- end model -->
	 
	<!-- /Add user Modal -->


	 <!-- Edit city Modal -->
	 <div class="modal fade" id="exampleModal2" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
		<div class="modal-dialog" role="document">
				<div class="modal-content">
						<div class="modal-header">	
								<h5 class="modal-title" id="exampleModalLabel"> اضافة  للمنتج : <span class="userName"></span> </h5>
						</div>
						<div class="modal-body">
								<form action="{{route('add-addtion-product')}}" method="post" enctype="multipart/form-data">

										<!-- token and user id -->
										{{csrf_field()}}
										<input type="hidden" name="add_product_id" value="">
										<input type="hidden" name="add_shop_id" value="">
										<!-- /token and user id -->
										<div class="row">
												<div class="row">
													<div class="col-sm-3 mt-1">
														<label>الاضافه</label>
													</div>
													<div class="col-sm-9 .mt-1">
														<select name="addtion_id" class=" form-control" id="addtion">
															
														</select>
														<p class='mt-1 messg ' styule="display:none">لايوجد اضافات متوفره</p>
													</div>
												</div>
												<div class="row">
													<div class="col-sm-3 mt-1">
														<label>الكميه</label>
													</div>
													<div class="col-sm-9 mt-1">
														<input type="number" name="quantity" class="form-control">
													</div>
												</div>
												<div class="row">
													<div class="col-sm-3 mt-1">
														<label>السعر</label>
													</div>
													<div class="col-sm-9 mt-1">
														<input type="number" name="price" class="form-control">
													</div>
												</div>
												<div class="row">
													<div class="col-sm-3 mt-1">
														<label> ملاحظات</label>
													</div>
													<div class="col-sm-9 mt-1">
														<input type="text" name="note" class="form-control">
													</div>
												</div>
										</div>
										<div class="row">
												<div class="col-sm-12" style="margin-top: 10px">
														<button type="submit" class="btn btn-primary" >حفظ الاضافه</button>
														<button type="button" class="btn btn-secondary" data-dismiss="modal">أغلاق</button>
												</div>
										</div>
										
								</form>
								<div class="mt-1 text-center">
										<h3 class="text-center">الاضافات الحاليه</h3>
										<div class="hasaddtion"></div>
								</div>
						</div>
				</div>
		</div>
</div>
<!-- /Edit city Modal -->
 <!-- Edit sizes Modal -->
 <div class="modal fade" id="exampleModal3" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
	<div class="modal-dialog" role="document">
			<div class="modal-content">
					<div class="modal-header">	
							<h5 class="modal-title" id="exampleModalLabel"> اضافة  حجم للمنتج : <span class="userName"></span> </h5>
					</div>
					<div class="modal-body">
							<form action="{{route('add-size-product')}}" method="post" enctype="multipart/form-data">

									<!-- token and user id -->
									{{csrf_field()}}
									<input type="hidden" name="size_product_id" value="">
									<input type="hidden" name="size_shop_id" value="">
									<!-- /token and user id -->
									<div class="row">
											<div class="row">
												<div class="col-sm-3 mt-1">
													<label>الحجم</label>
												</div>
												<div class="col-sm-9 .mt-1">
													<input type="text" name="size" placeholder="الحجم" class="form-control">
												</div>
											</div>
											<div class="row">
												<div class="col-sm-3 mt-1">
													<label>الكميه</label>
												</div>
												<div class="col-sm-9 mt-1">
													<input type="number" name="quantity" placeholder=" الكميه" class="form-control">
												</div>
											</div>
											<div class="row">
												<div class="col-sm-3 mt-1">
													<label> السعر</label>
												</div>
												<div class="col-sm-9 mt-1">
													<input type="number" name="price"  placeholder=" السعر" class="form-control">
												</div>
											</div>
									</div>
									<div class="row">
											<div class="col-sm-12" style="margin-top: 10px">
													<button type="submit" class="btn btn-primary" >حفظ الحجم</button>
													<button type="button" class="btn btn-secondary" data-dismiss="modal">أغلاق</button>
											</div>
									</div>
									
							</form>
							<div class="mt-1 text-center">
									<h3 class="text-center">الاحجام الحاليه</h3>
									<div class="hassize"></div>
							</div>
					</div>
			</div>
	</div>
</div>
<!-- /Edit city Modal -->



		</div>


</div>

<!-- javascript -->
@section('script')
<script type="text/javascript" src="{{asset('dashboard/js/plugins/tables/datatables/datatables.min.js')}}"></script>
<script type="text/javascript" src="{{asset('dashboard/js/plugins/forms/selects/select2.min.js')}}"></script>
<script type="text/javascript" src="{{asset('dashboard/js/pages/datatables_basic.js')}}"></script>
@endsection

<!-- script ajex -->
<script type="text/javascript">
    $("#catAjex").change(function(){
        // alert($(this).val())
    })
</script>

<script type="text/javascript">

	$('.openEditmodal').on('click',function(){
			//get valus
			var shop_id            = $(this).data('shop_id')
			var product_id      	 = $(this).data('product_id')
			var addtions       		 = $(this).data('addtions')
			var Hasaddtions        = $(this).data('hasaddtions')
		
			var flag = true;
			var flag2 = true;
			var table = `<table class="table ">
											<thead>
											<th>الاسم</th>
											<th>الكميه</th>
											<th>السعر</th>
											</thead>
											<tbody class='text-left'>`
				//set values in modal inputs
				console.log(addtions);
			$("input[name='add_shop_id']")           .val(shop_id)
			$("input[name='add_product_id']")         .val(product_id)
			$("#addtion").fadeOut()
			if(addtions && addtions.length > 0)
			{
			
					for (const addtion of addtions) {
						let findAddtion = Hasaddtions.find( cat => cat.addtion_id == addtion.id);
						if( findAddtion )
						{
							console.log("in first")
								flag   = false;  
								let tr = `<tr>
																<td>${addtion.name_ar}</td>
																<td>${findAddtion.quantity}</td>
																<td>${findAddtion.price} ريال</td>
																
													</tr>`
								table += tr;
						}
						else
						{
							let option = `<option value="${addtion.id}">${addtion.name_ar}</option>`
							$("#addtion").append(option);
							flag2 = false;
						
						}
						
				}
				table += "</tbody></table>"
				$(".hasaddtion").html(table);
			}
			else
			{
				if(flag)
							$(".hasaddtion").html("<p class='mt-1'>لايوجد اضافات متوفره</p>");
					
				$("#addtion").fadeOut()
				$("#addtion").next("p").fadeIn()
				
			}
			if(flag2)
			{
				$("#addtion").fadeOut()
				$("#addtion").next("p").fadeIn()
			}
			else
			{
				$("#addtion").fadeIn()
				$("#addtion").next("p").fadeOut();
			}
	
	})

 
	$('.openEditmodal2').on('click',function(){
			var shop_id            = $(this).data('shop_id')
			var product_id      	 = $(this).data('product_id')
			var sizes			     	   = $(this).data('sizes')
			
			var table = `<table class="table ">
											<thead>
											<th>الحجم</th>
											<th>الكميه</th>
											<th>السعر</th>
											</thead>
											<tbody class='text-left'>`
				//set values in modal inputs
			for (const size of sizes) {
				let tr = `<tr>
																<td>${size.size}</td>
																<td>${size.quantity}</td>
																<td>${size.price} ريال</td>
																
													</tr>`
								table += tr;
			}
			$("input[name='size_shop_id']")           .val(shop_id)
			$("input[name='size_product_id']")        .val(product_id)
			table += "</tbody></table>"
			$(".hassize").html(table);
	}
	)
</script>

{{-- edit  --}}

<script type="text/javascript">



	


</script>

<!-- other code -->
<script type="text/javascript">
	function addChooseFile(){$("input[name='image']").click()}

	//stay in current tab after reload
	$(function() { 
	    // for bootstrap 3 use 'shown.bs.tab', for bootstrap 2 use 'shown' in the next line
	    $('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
	        // save the latest tab; use cookies if you like 'em better:
	        localStorage.setItem('lastTab', $(this).attr('href'));
	    });

	    // go to the latest tab, if it exists:
	    var lastTab = localStorage.getItem('lastTab');
	    if (lastTab) {
	        $('[href="' + lastTab + '"]').tab('show');
	    }
	});

</script>
<!-- /other code -->


@endsection