<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateShopsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('shops', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name_ar');
            $table->string('name_en');
            $table->string('image')->default('default.png');
            $table->float("lat")->nullable();
            $table->float("lng")->nullable();
            $table->text("desc_ar")->nullable();
            $table->text("desc_en")->nullable();
            $table->time("start_time");
            $table->time("end_time");
            $table->enum('delivery', ['yes', 'no'])->default("no");
            $table->string("delivery_postion")->default("لايوجد توصيل");
            $table->float("delivery_coast")->default("0");
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('shops');
    }
}
