<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Shop_activity extends Model
{
    //
    protected $table = 'shop_activity';
    #Shop
    public function Shop()
    {
        return $this->belongsTo('App\Shop');
    }
    #Activity
    public function Activity()
    {
        return $this->belongsTo('App\Activity');
    }
   
}
