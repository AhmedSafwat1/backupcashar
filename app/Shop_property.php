<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Shop_property extends Model
{
    //
    protected $table = 'shop_property';
    protected $fillable = ['shop_id', 'status', 'property_id'];
    #Shop
    public function Shop()
    {
        return $this->belongsTo('App\Shop');
    }
    #Property
    public function Property()
    {
        return $this->belongsTo('App\Property');
    }
}
