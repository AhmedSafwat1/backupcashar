<?php

namespace App\Http\Controllers;


use Auth;
use File;
use Image;
use Session;
use App\Shop;
use App\Activity;
use App\Category;
use App\Property;
use App\Shop_property;
use Illuminate\Http\Request;

class ShopController extends Controller
{
    //
    public function shops()
    {
        
        $shops       = Shop::all();
        $categories  = Category::all();
        $activities  = Activity::all();
        $properties  = Property::all();
        return view('dashboard.shops.shops',compact('shops',"categories" ,"activities","properties" ));
    }
    // add Shop
    public function add(Request $request)
    {
        
        // dd($request->all());
        $propertiesAll  = Property::all();
        $niceError = array(
            'lng.required'              => " العنوان مطلوب يجيب اختياره من على الخريطه ",
            "name_ar.required"          => "الاسم باللغه العربيه مطلوب",
            "name_en.required"          => "الاسم باللغه الانجليزيه مطلوب",
            "edit_name_ar.min"          => "الاسم باللغه العربيه  لايقل عن 3 حرفين",
            "edit_name_en.min"          => "الاسم باللغه الانجليزيه لايقل عن 3 حرفين",
            "edit_name_ar.max"          => "الاسم باللغه العربيه لايزيد عن 190",
            "edit_name_en.min"          => "الاسم باللغه الانجليزيه لايزيد عن 190",
            "image.required"            => "الصوره مطلوبه",
            'start_time.required'       =>  ' وقت بدء العمل مطلوب ',
            'end_time.required'         =>  'وقت بدء العمل مطلوب ' ,
            'delivery.required'         =>  'يجب اختيار حالة خدمة التوصيل' ,
            'delivery.in'               => "يجب اختيار من الاختيارات المتاحه",
            'delivery_coast.required_if'=>"يجب تحدد ثمن خدمة التوصيل",
            "categories.required"       => "الاصناف المتاحه مطلوبه",
            "categories.min     "       => "  يجب اختيار صنف على الاقل",
            "categories.*.exists"       => " صنف  غير موجود ",
            "activities.required"       => "الانشطه المتاحه مطلوبه",
            "activities.min     "       => "  يجب اختيار نشاط على الاقل",
            "activities.*.exists"       => " النشاط  غير موجود ",
            "properties.*.exists"       => " خدمه الاضافيه  غير موجود "
        );
        $this->validate($request,[
            'name_ar'           =>'required|min:3|max:190',
            'name_en'           =>'required|min:3|max:190',
            'image'             =>'required|image',
            'lng'               => 'required',
            'start_time'        => 'required',
            'end_time'          => 'required',
            'delivery'          => 'required|in:yes,no',
            'delivery_coast'    => "nullable|numeric|required_if:delivery,==,yes",
            'delivery_postion'  => "nullable",
            'categories'        =>"required|array|min:1",
            'categories.*'      =>"distinct|required|exists:categories,id",
            'activities'        =>"required|array|min:1",
            'activities.*'      =>"distinct|required|exists:activities,id",
            "properties"        =>"nullable|array|min:0",
            'properties.*'      =>"distinct|exists:properties,id"

        ], $niceError);
        
        $shop                =new Shop;
        $shop->name_ar       =$request->name_ar;
        $shop->name_en       =$request->name_en;
        $shop->desc_ar       =$request->desc_ar;
        $shop->desc_en       =$request->desc_en;
        $shop->lat           =$request->lat;
        $shop->lng           =$request->lng;
        $shop->start_time    =$request->start_time;
        $shop->end_time      =$request->end_time; 
        $shop->delivery      =$request->delivery;
        if($request->delivery === "yes"  && isset($request->delivery_coast))
        {
            $shop->delivery_coast    = $request->delivery_coast;
            $shop->delivery_postion  = $request->delivery_postion? $request->delivery_postion:"";
        }
        // dd($shop);
        $photo=$request->image;
        $name=date('d-m-y').time().rand().'.'.$photo->getClientOriginalExtension();
        Image::make($photo)->save('dashboard/uploads/shops/'.$name);
        $shop->image=$name;
        $shop->save();
        $shop->Categories()->attach($request->categories);
        $shop->Activities()->attach($request->activities);
        if(is_array($request->properties))
        {
            foreach ($propertiesAll as  $propy) {
                $status      = in_array($propy->id, $request->properties);
                $data = array(
                    "status"        => $status,
                    "shop_id"       => $shop->id,
                    "property_id"   => $propy->id
                );
                Shop_property::create($data);
            }
        }
        Report(Auth::user()->id,'بأضافة مطعم جديد');
        Session::flash('success','تم اضافة المطعم');
        // dd($shop);
        return back();
        
    }
    // delete Shop
    public function delete(Request $request)
    {
        // dd($request->all());
        $Shop = Shop::findOrFail($request->id);
        if($Shop->image != 'default.png')
        File::delete('dashboard/uploads/shops/'.$Shop->image );
        $Shop->delete();    

        Report(Auth::user()->id,'بحذف المطعم '.$Shop->name_ar);
        Session::flash('success','تم الحذف');
        return back();
    }
    public function edit(Request $request)
    {
       
        // dd($request->all()); 
        $propertiesAll  = Property::all();
        $niceError = array(
            'edit_lat.required'              => " العنوان مطلوب يجيب اختياره من على الخريطه ",
            "edit_name_ar.required"          => "الاسم باللغه العربيه مطلوب",
            "edit_name_en.required"          => "الاسم باللغه الانجليزيه مطلوب",
            "edit_name_ar.min"               => "الاسم باللغه العربيه  لايقل عن 3 حرفين",
            "edit_name_en.min"               => "الاسم باللغه الانجليزيه لايقل عن 3 حرفين",
            "edit_name_ar.max"               => "الاسم باللغه العربيه لايزيد عن 190",
            "edit_name_en.min"               => "الاسم باللغه الانجليزيه لايزيد عن 190",
            "edit_image.required"            => "الصوره مطلوبه",
            'edit_start_time.required'       =>  ' وقت بدء العمل مطلوب ',
            'edit_end_time.required'         =>  'وقت بدء العمل مطلوب ' ,
            'delivery.required'              =>  'يجب اختيار حالة خدمة التوصيل' ,
            'delivery.in'                    => "يجب اختيار من الاختيارات المتاحه",
            'edit_delivery_coast.required_if'=>"يجب تحدد ثمن خدمة التوصيل",
            "id.required"                    =>"رقم التعريفى مطلوب",
            "edit_categories.min     "       => "  يجب اختيار صنف على الاقل",
            "edit_categories.*.exists"       => " صنف  غير موجود ",
            "edit_activities.required"       => "الانشطه المتاحه مطلوبه",
            "edit_activities.min     "       => "  يجب اختيار نشاط على الاقل",
            "edit_activities.*.exists"       => " النشاط  غير موجود "
        );
        $this->validate($request,[
            'edit_name_ar'           =>'required|min:3|max:190',
            'edit_name_en'           =>'required|min:3|max:190',
            'edit_image'             =>'nullable|image',
            'edit_lat'               => 'required',
            'edit_start_time'        => 'required',
            'edit_end_time'          => 'required',
            'delivery'               => 'required|in:yes,no',
            'edit_delivery_coast'    => "nullable|numeric|required_if:delivery,==,yes",
            'edit_delivery_postion'  => "nullable",
            "id"                     =>"required",
            'edit_categories'        =>"required||array|min:1",
            'edit_categories.*'      =>"distinct|required|exists:categories,id",
            'edit_activities.*'      =>"distinct|required|exists:activities,id",
            "edit_properties"            =>"nullable|array|min:0",
            'edit_properties.*'          =>"distinct|exists:properties,id"

        ], $niceError);
        $shop                = Shop::findOrFail($request->id);
        $shop->name_ar       =$request->edit_name_ar;
        $shop->name_en       =$request->edit_name_en;
        $shop->desc_ar       =$request->edit_desc_ar;
        $shop->desc_en       =$request->edit_desc_en;
        $shop->lat           =$request->edit_lat;
        $shop->lng           =$request->edit_lng;
        $shop->start_time    = $request->edit_start_time;
        $shop->end_time      = $request->edit_end_time; 
        $shop->delivery      = $request->delivery;
        if($request->delivery === "yes")
        {
            $shop->delivery_coast    = $request->edit_delivery_coast;
            $shop->delivery_postion  = $request->edit_delivery_postion? $request->edit_delivery_postion:"";
        }
        else {
            $shop->delivery_coast    = 0;
            $shop->delivery_postion  = "لا يوجد";
        }
        if(!empty($request->edit_image))
        {
            $photo=$request->edit_image;
            $name=date('d-m-y').time().rand().'.'.$photo->getClientOriginalExtension();
            if($shop->image != 'default.png')
            {
                File::delete('dashboard/uploads/shops/'.$shop->image);
                Image::make($photo)->save('dashboard/uploads/shops/'.$name);
            }
            else
            {
                Image::make($photo)->save('dashboard/uploads/shops/'.$name);
            }
            $shop->image=$name;   
        }
        // dd($shop)
        $categories = Category::select("id")->get();
        // categories
        $shop->Categories()->detach($categories);
        $shop->Categories()->attach($request->edit_categories);
        $activities = Activity::select("id")->get();
        // activities;
        $shop->Activities()->detach($activities);
        $shop->Activities()->attach($request->edit_activities);
        //property
        $shop->ShopProperties()->delete();
        if(is_array($request->edit_properties))
        {
            foreach ($propertiesAll as  $propy) {
                $status      = in_array($propy->id, $request->edit_properties);
                $data = array(
                    "status"        => $status,
                    "shop_id"       => $shop->id,
                    "property_id"   => $propy->id
                );
                Shop_property::create($data);
            }
        }
        $shop->save();
        
        Report(Auth::user()->id,'بتحديث بيانات  '.$shop->name_ar);
        Session::flash('success','تم حفظ التعديلات');
        // dd($shop);
        return back();
    }
}
