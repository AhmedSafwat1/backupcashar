<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Category;
use Auth;
use Session;
use Image;
use File;
class CategoryController extends Controller
{
    //
    public function  __construct()
    {
      
    }
    // display categories
    public function categories()
    {
        
        $categories = Category::all();
        // dd($categories);

        return view('dashboard.categories.categories',compact('categories'));
    }
    // add categories
    public function add(Request $request)
    {
        $customError  = array(
            'name_ar.required' => "اسم الصنف باللغه العربيه مطلوب", 
            'name_en.required' => "اسم الصنف باللغه الانجليزىه  مطلوب",
            'code.required'    => " كود الصنف مطلوب ",
            'code.unique'      => "كود الصنف موجود سابقا ",
            'name_ar.min'      => " اسم الصنف باللغه العربيه لايقل عن 3 حروف", 
            'name_en.min'      => " اسم الصنف باللغه الانجليزيه لايقل عن 3 حروف", 
            'name_ar.max'      => " اسم الصنف باللغه العربيه لايزيد عن 190 حروف", 
            'name_en.max'      => " اسم الصنف باللغه الانجليزيه  لايزيد عن 190 حروف", 
            "icon.required"    => "الصوره مطلوبه",
            "icon.image"       => "الصوره مطلوبه يجب ان تكون صوره",
          
        );
        $this->validate($request, [
            'name_ar' => 'required|min:3|max:190',
            'name_en' => 'required|min:3|max:190',
            'icon'    =>'required|image',
        ], $customError);
        $photo=$request->icon;
        $name=date('d-m-y').time().rand().'.'.$photo->getClientOriginalExtension();
        Image::make($photo)->save('dashboard/uploads/categories/'.$name);
       
        $category                = new Category;
        $category->icon          =$name;
        $category->name_ar       =$request->name_ar;
        $category->name_en       =$request->name_en;
        $category->save();
        Report(Auth::user()->id,'بأضافة صنف جديد');
        Session::flash('success','تم اضافة الصنف');
		return back();
    }
    // delete category
    public function delete(Request $request)
    {
        // dd($request->all());
        $category = Category::findOrFail($request->id);
        if($category->image != 'default.png')
             File::delete('dashboard/uploads/categories/'.$category->image );
        $category->delete();
        Report(Auth::user()->id,'بحذف الصنف '.$category->name);
        Session::flash('success','تم الحذف');
        return back();
    }
    public function edit(Request $request)
    {
        $category = Category::findOrFail($request->id);
        $customError  = array(
            'edit_name_ar.required' => "اسم الصنف باللغه العربيه مطلوب" , 
            'edit_name_en.required' => "اسم الصنف باللغه الانجليزىه  مطلوب",
            'edit_name_ar.min'      => " اسم الصنف باللغه العربيه لايقل عن 3 حروف", 
            'edit_name_en.min'      => " اسم الصنف باللغه الانجليزيه لايقل عن 3 حروف",
            'edit_name_ar.max'      => " اسم الصنف باللغه العربيه لايزيد عن 190 حروف", 
            'edit_name_en.max'      => " اسم الصنف باللغه الانجليزيه  لايزيد عن 190 حروف",  
            'edit_name_ar.unique'   => " اسم الصنف باللغه العربيه موجود مسبقا", 
            'edit_name_en.unique'   => " اسم الصنف باللغه الانجليزيه موجود مسبقا", 
            
        );
        $this->validate($request, [
            'edit_name_ar' => 'required|min:3|max:190|unique:categories,name_ar,'.$category["id"],
            'edit_name_en' => 'required|min:3|max:190|unique:categories,name_en,'.$category["id"],
            
        ], $customError);
      
      
        $category->name_ar = $request->edit_name_ar;
        $category->name_en = $request->edit_name_en;
        if(!empty($request->edit_icon))
        {
            $photo=$request->edit_icon;
            $name=date('d-m-y').time().rand().'.'.$photo->getClientOriginalExtension();
            if($category->icon != 'default.png')
            {
                File::delete('dashboard/uploads/categories/'.$category->icon);
                Image::make($photo)->save('dashboard/uploads/categories/'.$name);
            }
            else
            {
                Image::make($photo)->save('dashboard/uploads/categories/'.$name);
            }
            $category->icon=$name;   
        }
        $category->save();
        
        Report(Auth::user()->id,'قام بتعديل الصنف '.$category->name);
        Session::flash('success','تم تعديل');
        return back();
    }
}
