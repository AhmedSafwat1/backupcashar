<?php

namespace App\Http\Controllers;



use Illuminate\Http\Request;

use App\City;
use Session;
use Auth;

class CityController extends Controller
{
    public function  __construct()
    {
        \Carbon::setLocale('ar');
    }
    // display cities
    public function cities()
    {
        
        $cities = City::where('show','=',true)->get();

        return view('dashboard.cities.cities',compact('cities'));
    }
    // add city
    public function add(Request $request)
    {
        $customError  = array(
            'name_ar.required' => "اسم البلد باللغه العربيه مطلوب", 
            'name_en.required' => "اسم البلد باللغه الانجليزىه  مطلوب",
            'code.required'    => " كود البلد مطلوب ",
            'code.unique'      => "كود البلد موجود سابقا ",
            'name_ar.min'      => " اسم البلد باللغه العربيه لايقل عن 3 حروف", 
            'name_en.min'      => " اسم البلد باللغه الانجليزيه لايقل عن 3 حروف", 
            'name_ar.max'      => " اسم البلد باللغه العربيه  لايزيد عن 190 حروف", 
            'name_en.max'      => " اسم البلد باللغه الانجليزيه  لايزيد عن 190 حروف", 
            'code.min'         => " كود البلد لايقل عن 3 ارقام", 
            'code.numeric'     => " كود البلد يتكون من ارقام فقط", 
          
        );
        $this->validate($request, [
            'name_ar' => 'required|min:3|max:190',
            'name_en' => 'required|min:3|max:190',
            'code' => 'required|min:3|numeric|unique:cities,code',
        ], $customError);
        
        City::create($request->all());
        Report(Auth::user()->id,'بأضافة بلد جديد');
        Session::flash('success','تم اضافة البلد');
		return back();
    }
    // delete city
    public function delete(Request $request)
    {
        // dd($request->all());
        $city = City::findOrFail($request->id);
        $city->show = false;
        $city->save();
        Report(Auth::user()->id,'بحذف البلد '.$city->name);
        Session::flash('success','تم الحذف');
        return back();
    }
    public function edit(Request $request)
    {
        $city = City::findOrFail($request->id);
        $customError  = array(
            'edit_name_ar.required' => "اسم البلد باللغه العربيه مطلوب" , 
            'edit_name_en.required' => "اسم البلد باللغه الانجليزىه  مطلوب",
            'edit_code.required'    => " كود البلد مطلوب ",
            'edit_code.unique'      => "كود البلد موجود سابقا",
            'edit_name_ar.min'      => " اسم البلد باللغه العربيه لايقل عن 3 حروف", 
            'edit_name_en.min'      => " اسم البلد باللغه الانجليزيه لايقل عن 3 حروف", 
            'edit_code.min'         => " كود البلد لايقل عن 3 ارقام", 
            'edit_code.numeric'     => " كود البلد يتكون من ارقام فقط", 
            
        );
        $this->validate($request, [
            'edit_name_ar' => 'required|min:3',
            'edit_name_en' => 'required|min:3',
            'edit_code'    => 'required|min:3|numeric|unique:cities,code,'.$city["id"],
        ], $customError);
      
      
        $city->name_ar = $request->edit_name_ar;
        $city->name_en = $request->edit_name_en;
        $city->code    = $request->edit_code;
        $city->save();
        
        Report(Auth::user()->id,'قام بتعديل البلد '.$city->name);
        Session::flash('success','تم تعديل');
        return back();
    }

}

