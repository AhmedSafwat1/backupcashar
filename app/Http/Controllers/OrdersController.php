<?php

namespace App\Http\Controllers;

use App\Order;
use Illuminate\Http\Request;

class OrdersController extends Controller
{
    // filter collection basic count > 0
    public function filter($orders)
    {
        return $orders->filter(function($item)
        {
            if($item->cards_count > 0)
            {
                return $item;
            }
        });
    }
    // order all 
    public function Orders(Request $request)
    {
        $orders = Order::withCount("Cards")->get();
        $orders = $this->filter($orders);
        $name = "الحاليه";
        return view('dashboard.orders.orders',compact('orders',"name"));
    }
    // order new 
    public function newOrders(Request $request)
    {
        $orders = Order::whereStatus(1)->withCount("Cards")->get();
        $orders = $this->filter($orders);
        $name = "الجديده";
        return view('dashboard.orders.orders',compact('orders',"name"));
    }
    // order accept 
    public function acceptOrders(Request $request)
    {
        $orders = Order::whereStatus(2)->withCount("Cards")->get();
        $orders = $this->filter($orders);
        $name = "المقبوله";
        return view('dashboard.orders.orders',compact('orders',"name"));
    }
    // order ready 
    public function readyOrders(Request $request)
    {
        $orders = Order::whereStatus(3)->withCount("Cards")->get();
        $orders = $this->filter($orders);
        $name   = "الجاهزه";
        return view('dashboard.orders.orders',compact('orders',"name"));
    }
    // order delivery 
    public function deliveryOrders(Request $request)
    {
        $orders = Order::whereStatus(4)->withCount("Cards")->get();
        $orders = $this->filter($orders);
        $name   = "الجاهزه";
        return view('dashboard.orders.orders',compact('orders',"name"));
    }
    // order reach 
    public function reachOrders(Request $request)
    {
        $orders = Order::whereStatus(5)->withCount("Cards")->get();
        $orders = $this->filter($orders);
        $name   = "الجاهزه";
        return view('dashboard.orders.orders',compact('orders',"name"));
    }
    // order reach 
    public function refuseOrders(Request $request)
    {
        $orders = Order::whereStatus(6)->withCount("Cards")->get();
        $orders = $this->filter($orders);
        $name   = "المرفوضه";
        return view('dashboard.orders.orders',compact('orders',"name"));
    }
    // order show 
    public function ShowOrder(Request $request, $id)
    {
        $order = Order::findOrFail($id);
        // dd($order->id);
        return view('dashboard.orders.details',compact('order'));
    }
    // order accept 
    public function actionOrder(Request $request, $id, $state)
    {
        // dd($id);
        $order = Order::findOrFail($id);
       
        if(!is_numeric($state) || ($state > 6 && $state < 0))
            abort("404");
        $order->status =  $state;
        $order->save();
        // dd($order->id);
        Report(\Auth::user()->id,'   بتغير حالة الطلب ل'.$order->User->name);
        \Session::flash('success','تم تغير الحاله');
		return back();
        
    }
}
