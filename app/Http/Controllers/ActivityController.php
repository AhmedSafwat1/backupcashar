<?php

namespace App\Http\Controllers;

use Auth;
use Session;
use App\Activity;
use Illuminate\Http\Request;

class ActivityController extends Controller
{
    //
    public function  __construct()
    {
       
    }
    //
    // display activities
    public function activities()
    {
        
        $activities   = Activity::all();
        return view('dashboard.activities.activities',compact('activities'));
    }
    // add Activity
    public function add(Request $request)
    {
        // dd($request->all());
        $customError  = array(
            'name_ar.required'       => "اسم النشاط باللغه العربيه مطلوب", 
            'name_en.required'       => "اسم النشاط باللغه الانجليزىه  مطلوب",
            'name_ar.min'            => " اسم النشاط باللغه العربيه لايقل عن 3 حروف", 
            'name_en.min'            => " اسم النشاط باللغه الانجليزيه لايقل عن 3 حروف",
            'name_ar.max'            => " اسم النشاط باللغه العربيه لايزيد عن 190 حروف", 
            'name_en.max'            => " اسم النشاط باللغه الانجليزيه لايزيد عن 190 حروف", 
            
        );
        $this->validate($request, [
            'name_ar' => 'required|min:3|max:190',
            'name_en' => 'required|min:3|max:190',
        
        ], $customError);
        
        Activity::create($request->all());
        Report(Auth::user()->id,'بأضافة نشاط جديد');
        Session::flash('success','تم اضافة نشاط');
		return back();
    }
    // delete Activity
    public function delete(Request $request)
    {
        // dd($request->all());
        $Activity = Activity::findOrFail($request->id);
        $Activity->delete() ;
        Report(Auth::user()->id,'بحذف النشاط '.$Activity->name);
        Session::flash('success','تم الحذف');
        return back();
    }
    public function edit(Request $request)
    {
        $Activity = Activity::findOrFail($request->id);
        
        $customError  = array(
            'edit_name_ar.required'       => "اسم النشاط باللغه العربيه مطلوب", 
            'edit_name_en.required'       => "اسم النشاط باللغه الانجليزىه  مطلوب",
            'edit_name_ar.min'            => " اسم النشاط باللغه العربيه لايقل عن 3 حروف", 
            'edit_name_en.min'            => " اسم النشاط باللغه الانجليزيه لايقل عن 3 حروف",
            'edit_name_ar.max'            => " اسم النشاط باللغه العربيه لايزيد عن 190 حروف", 
            'edit_name_en.max'            => " اسم النشاط باللغه الانجليزيه لايزيد عن 190 حروف", 
        );
        $this->validate($request, [
            'edit_name_ar' => 'required|min:3|max:190',
            'edit_name_en' => 'required|min:3|max:190',
        ], $customError);
      
      
        $Activity->name_ar = $request->edit_name_ar;
        $Activity->name_en = $request->edit_name_en;
        $Activity->save();
        
        Report(Auth::user()->id,'قام بتعديل النشاط '.$Activity->name_ar);
        Session::flash('success','تم تعديل');
        return back();
    }
}
