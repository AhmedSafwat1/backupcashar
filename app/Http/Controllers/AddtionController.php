<?php

namespace App\Http\Controllers;

use App\Addtion;
use App\Category;
use Session;
use Auth;
use Illuminate\Http\Request;

class AddtionController extends Controller
{
    //
    public function  __construct()
    {
        \Carbon::setLocale('ar');
    }
    // display addtions
    public function addtions()
    {
        
        $addtions   = Addtion::all();
        $categories = Category::all();
     
        return view('dashboard.addtions.addtions',compact('addtions', "categories"));
    }
    // add Addtion
    public function add(Request $request)
    {
        // dd($request->all());
        $customError  = array(
            'name_ar.required'       => "اسم لاضافه باللغه العربيه مطلوب", 
            'name_en.required'       => "اسم لاضافه باللغه الانجليزىه  مطلوب",
            'category_id.required'   => " صنف  الاضافه مطلوب ",
            'category_id.exists'     => " صنف  الاضافه غير موجود ",
            'name_ar.min'            => " اسم لاضافه باللغه العربيه لايقل عن 3 حروف", 
            'name_en.min'            => " اسم لاضافه باللغه الانجليزيه لايقل عن 3 حروف", 
            'name_ar.max'            => " اسم لاضافه باللغه العربيه لايزيد عن 190 حروف", 
            'name_en.max'            => " اسم لاضافه باللغه الانجليزيه  لايزيد عن 190 حروف",
        );
        $this->validate($request, [
            'name_ar' => 'required|min:3|max:190',
            'name_en' => 'required|min:3|max:190',
            'category_id' => 'required|exists:categories,id',
           
        ], $customError);
        
        Addtion::create($request->all());
        Report(Auth::user()->id,'بأضافة اضافه جديد');
        Session::flash('success','تم اضافة لاضافه');
		return back();
    }
    // delete Addtion
    public function delete(Request $request)
    {
        // dd($request->all());
        $Addtion = Addtion::findOrFail($request->id);
        $Addtion->delete() ;
        Report(Auth::user()->id,'بحذف لاضافه '.$Addtion->name_ar);
        Session::flash('success','تم الحذف');
        return back();
    }
    public function edit(Request $request)
    {
        $Addtion = Addtion::findOrFail($request->id);
        // dd($request->all());
        // $category = Category::findOrFail($request->category_id);
        $customError  = array(
            'edit_name_ar.required'       => "اسم لاضافه باللغه العربيه مطلوب", 
            'edit_name_en.required'       => "اسم لاضافه باللغه الانجليزىه  مطلوب",
            'category_id.required'        => " صنف  الاضافه مطلوب ",
            'edit_name_ar.min'            => " اسم لاضافه باللغه العربيه لايقل عن 3 حروف", 
            'edit_name_en.min'            => " اسم لاضافه باللغه الانجليزيه لايقل عن 3 حروف", 
            'category_id.exists'          => " صنف  الاضافه غير موجود ",
            'edit_name_ar.max'            => " اسم لاضافه باللغه العربيه لايزيد عن 190 حروف", 
            'edit_name_en.max'            => " اسم لاضافه باللغه الانجليزيه  لايزيد عن 190 حروف",
            
        );
        $this->validate($request, [
            'edit_name_ar' => 'required|min:3|max:190',
            'edit_name_en' => 'required|min:3|max:190',
            'category_id' => 'required|exists:categories,id',
           
        ], $customError);
      
      
        $Addtion->name_ar = $request->edit_name_ar;
        $Addtion->name_en = $request->edit_name_en;
        
        $Addtion->category_id = $request->category_id;
        $Addtion->save();
        
        Report(Auth::user()->id,'قام بتعديل لاضافه '.$Addtion->name);
        Session::flash('success','تم تعديل');
        return back();
    }
}
