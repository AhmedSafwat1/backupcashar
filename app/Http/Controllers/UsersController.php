<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Mail\PublicMessage;
use App\SmsEmailNotification;
use Mail;
use Auth;
use App\User;
use App\Role;
use App\City;
use Image;
use File;
use Session;

class UsersController extends Controller
{
    #users page
    public function Users()
    {
        $users = User::with('Role')->latest()->get();
        $roles = Role::latest()->get();
        $cities = City::where('show','=',true)->get();
        // dd(compact('users',$users,'roles',$roles,'cities',$cities));
    	return view('dashboard.users.users',compact('users',$users,'roles',$roles,'cities',$cities));
    }

    #add user
    public function AddUser(Request $request)
    {
        // dd($request->all());
        $niceError = array      (
            'lng.required'               => " العنوان مطلوب يجيب اختياره من على الخريطه ", 
            'address.required'           => " العنوان مطلوب يجيب اختياره من على الخريطه ", 
            "city_id.required"           => "كود البلد مطلوب",
            "city_id.exists"             => "كود البلد غير موجود",
            "last.required"              => " الاسم الاخير مطلوب",
            "birth.required"             => "تاريخ الميلاد مطلوب",
            'phone.integer'              =>  ' رقم الجوال يجب ان يكون  ارقام ',
            'phone.digits_between'       =>  '  رقم الجوال يجب ان يكون  5 ارقام على الاقل او 40 على الاكثر',
            'phone.unique'               =>  'رقم الجوال مستخدم من قبل' ,
            'email.unique'               =>  'البريد الالكترونى مستخدم من قبل' ,
        );
        $this->validate($request,[
            'name'     =>'required|min:2|max:190',
            'email'    =>'required|email|min:2|unique:users,email',
            'phone'    =>'required|integer|digits_between:5,40|unique:users,phone',
            'avatar'   =>'required|image',
            'password' =>'required|min:6|max:190',
            'address'  => 'required',
            'lng'      => 'required',
            'address'  => 'required',
            'last'     => 'required',
            'birth'    => 'required',
            'city_id'  => "required|exists:cities,id"
        ], $niceError);
        
		$user=new User;
		$user->name       =$request->name;
		$user->email      =$request->email;
        $user->phone      =$request->phone;
        $user->role       =$request->role;

        if($request->active != "")
        {
            $user->active     =$request->active;
        }else
        {
            $user->active     =1;
        }
		
        $user->password   = bcrypt($request->password);
        $user->address    = $request->address;
        $user->last       = $request->last;
        $user->birth      = $request->birth;
        $user->city_id    = $request->city_id;
        $user->lat        = $request->lat;
        $user->lng        = $request->lng;
        // dd($user);
        $photo=$request->avatar;
        $name=date('d-m-y').time().rand().'.'.$photo->getClientOriginalExtension();
        Image::make($photo)->save('dashboard/uploads/users/'.$name);
        $user->avatar=$name;
		$user->save();
        Report(Auth::user()->id,'بأضافة عضو جديد');
		Session::flash('success','تم اضافة العضو');
		return back();
    }

    #update user
    public function UpdateUser(Request $request)
    {
        // dd($request->all());
        $user=User::findOrFail($request->id);
        $niceError = array(
            'edit_lng.required'          => " العنوان مطلوب يجيب اختياره من على الخريطه ", 
            'edit_address.required'      => " العنوان مطلوب يجيب اختياره من على الخريطه ", 
            "edit_city_id.required"      => "كود البلد مطلوب",
            "edit_last.required"         => " الاسم الاخير مطلوب",
            "edit_email.required"        => " البريد الاكترونى   مطلوب",
            "edit_birth.required"        => "تاريخ الميلاد مطلوب",
            'edit_phone.unique'          =>  'رقم الجوال مستخدم من قبل' ,
            'edit_email.unique'          =>  'البريد الالكترونى مستخدم من قبل' ,
            'edit_phone.integer'         =>  ' رقم الجوال يجب ان يكون  ارقام ',
            'edit_phone.digits_between'  =>  '  رقم الجوال يجب ان يكون  5 ارقام على الاقل او 40 على الاكثر',
        );
        $this->validate($request,[
            'edit_name'     =>'required|min:2|max:190',
            'edit_email'    =>'required|email|min:2|max:190|unique:users,email,'. $user["id"],
            'edit_phone'    =>'required|integer|digits_between:5,40|unique:users,phone,'. $user["id"],
            'edit_avatar'   =>'nullable|image',
            'password'      =>'nullable|min:6|max:190',
            'edit_lng'      => 'required',
            'edit_address'  => 'required',
            'edit_last'     => 'required',
            'edit_birth'    => 'required',
            'edit_city_id'  => "required|exists:cities,id"
        ], $niceError);

	
		$user->name       =$request->edit_name;

		if(User::where('email','=',$request->edit_email)->count() == 0 || $user->email === $request->edit_email)
        {
          $user->email =$request->edit_email;
        }

        if(User::where('phone','=',$request->edit_phone)->count() == 0 || $user->phone === $request->edit_phone)
        {
          $user->phone =$request->edit_phone;
        }


		if(!is_null($request->password))
		{
			$user->password =bcrypt($request->password);
        }
        
        if(!is_null($request->role))
		{
            if($user->id != 1)
            {
                $user->role =$request->role;
            }else
            {
                Session::flash('danger','لا يمكن تغير صلاحية هذا العضو');
            }
		}


        if(isset($request->active))
        {
            if($user->id != 1)
            {
                $user->active     =$request->active;
            }else
            {
                Session::flash('danger','لا يمكن حظر هذا العضو');
            }
        }else
        {
            $user->active     =1;
        }

        if(!empty($request->edit_avatar))
        {
            $photo=$request->edit_avatar;
            $name=date('d-m-y').time().rand().'.'.$photo->getClientOriginalExtension();
            if($user->avatar != 'default.png')
            {
                File::delete('dashboard/uploads/users/'.$user->avatar);
                Image::make($photo)->save('dashboard/uploads/users/'.$name);
            }
            else
            {
                Image::make($photo)->save('dashboard/uploads/users/'.$name);
            }
            $user->avatar=$name;   
        }
        $user->address    = $request->edit_address;
        $user->last       = $request->edit_last;
        $user->birth      = $request->edit_birth;
        $user->city_id    = $request->edit_city_id;
        $user->lat        = $request->edit_lat;
        $user->lng        = $request->edit_lng;
		$user->save();
        Report(Auth::user()->id,'بتحديث بيانات  '.$user->name);
		Session::flash('success','تم حفظ التعديلات');
		return back();
    }

    #delete user
    public function DeleteUser(Request $request)
    {
        if($request->id == 1)
        {
            Session::flash('danger','لا يمكن حذف هذا العضو');
            return back();
        }else
        {
            $user = User::findOrFail($request->id);
            File::delete('dashboard/uploads/users/'.$user->photo);
            $user->delete();
            Report(Auth::user()->id,'بحذف العضو '.$user->name);
            Session::flash('success','تم الحذف');
            return back();
        }
    }

    #email correspondent for all users
    public function EmailMessageAll(Request $request)
    {
        $this->validate($request,[
            'email_message' =>'required|min:1'
        ]);

        $checkConfig = SmsEmailNotification::first();
        if(
            $checkConfig->smtp_type         == "" ||
            $checkConfig->smtp_username     == "" ||
            $checkConfig->smtp_password     == "" ||
            $checkConfig->smtp_sender_email == "" ||
            $checkConfig->smtp_port         == "" ||
            $checkConfig->smtp_host         == ""
        ){
            dd($checkConfig->smtp_username );
            Session::flash('danger','لم يتم ارسال الرساله ! .. يرجى مراجعة بيانات ال SMTP');
            return back();
        }else
        {
            $users = User::get();
            foreach ($users as $u)
            {
                Mail::to($u->email)->send(new PublicMessage(  $request->email_message  ));
            }
            Session::flash('success','تم ارسال الرساله');
            return back();
        }
    }

    #sms correspondent for all users
    public function SmsMessageAll(Request $request)
    {
        $this->validate($request,[
            'sms_message' =>'required'
        ]);

        $users = User::get();
        foreach ($users as $u)
        {
            send_mobile_sms($u->phone,$request->sms_message);
        }
        
        Session::flash('success','تم ارسال الرساله');
        return back();
    }

    #notification correspondent for all users
    public function NotificationMessageAll(Request $request)
    {
        $this->validate($request,[
            'notification_message' =>'required'
        ]);

        $users = User::get();
        foreach ($users as $u)
        {
            #use FCM or One Signal Here :) 
        }
    }

    #end email for current user
    public function SendEmail(Request $request)
    {
        $this->validate($request,[
            'email_message' =>'required|min:1'
        ]);

        $checkConfig = SmsEmailNotification::first();
        if(
            $checkConfig->smtp_type         == "" ||
            $checkConfig->smtp_username     == "" ||
            $checkConfig->smtp_password     == "" ||
            $checkConfig->smtp_sender_email == "" ||
            $checkConfig->smtp_port         == "" ||
            $checkConfig->smtp_host         == ""
        ){
            Session::flash('danger','لم يتم ارسال الرساله ! .. يرجى مراجعة بيانات ال SMTP');
            return back();
        }else
        {
            Mail::to($request->email)->send(new PublicMessage($request->email_message));
            Session::flash('success','تم ارسال الرساله');
            return back();
        }
    }

    #send sms for current user
    public function SendSMS(Request $request)
    {
        $this->validate($request,[
            'sms_message' =>'required'
        ]);

        send_mobile_sms($request->phone,$request->sms_message);
        Session::flash('success','تم ارسال الرساله');
        return back();
    }

    #send notification for current user
    public function SendNotification (Request $request)
    {
        $this->validate($request,[
            'notification_message' =>'required'
        ]);

        #use FCM or One Signal Here :) 
    }
}
