<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MoneyTransfer extends Model
{
    //
    protected $table = 'money_transfers';

    public function User()
    {
        return $this->belongsTo('App\User','user_id','id');
    }
}
