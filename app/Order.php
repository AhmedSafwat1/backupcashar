<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
     // user have
     public function User()
     {
        return $this->belongsTo('App\User');
     }
     #card for users
     public function Cards()
     {
        return $this->hasMany('App\Card');
     }
}
