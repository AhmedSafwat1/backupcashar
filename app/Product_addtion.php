<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Product_addtion extends Model
{
    //
     #product
     public function Product()
     {
         return $this->belongsTo('App\Product');
     }
     #addtion
     public function Addtion()
     {
         return $this->belongsTo('App\Addtion');
     }
     #user
     public function User()
     {
         return $this->belongsTo('App\User');
     }
}
