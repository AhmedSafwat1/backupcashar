<?php

namespace App;

use App\Shop;
use App\Shop_property;
use Illuminate\Database\Eloquent\Model;

class Property extends Model
{
    // shop have the prperty
    protected $fillable = ['name_ar', 'name_en'];
    public function ShopsProperty()
    {
        return $this->hasMany(Shop_property::class);
    }
}
