<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Card extends Model
{
    // user have
    public function User()
    {
        return $this->belongsTo('App\User');
    }
    // user size
    public function Size()
    {
        return $this->belongsTo('App\Size');
    }
    // user order
    public function Oreder()
    {
        return $this->belongsTo('App\Order');
    }
}
