<?php

namespace App;
use App\Shop;
use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    protected $fillable = ['name_ar', 'name_en', 'code'];
    //shops
    public function Shops()
    {
        return $this->belongsToMany(Shop::class);
    }
    // has prodcut in category
    public function Products()
    {
        return $this->hasMany('App\Product');
    }
    // has prodcut in category
    public function Addtions()
    {
        return $this->hasMany('App\Addtion');
    }
    
}
