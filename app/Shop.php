<?php

namespace App;
use App\Product;
use App\Activity;
use App\Category;
use App\Property;
use App\Shop_activity;
use App\Shop_property;
use Illuminate\Database\Eloquent\Model;

class Shop extends Model
{
    //
    public function Categories()
    {
        return $this->belongsToMany('App\Category', 'category_shop')
                    ->withTimestamps();
        
    }
    public function ShopProperties()
    {
        return $this->hasMany(Shop_property::class);
    }
    public function Products()
    {
        return $this->hasMany(Product::class);
    }
    public function Activities()
    {
        return $this->belongsToMany('App\Activity', 'shop_activity')
                    ->withTimestamps();
    }
    public function ShopActivities()
    {
        return $this->hasMany(Shop_activity::class);
    }
}
