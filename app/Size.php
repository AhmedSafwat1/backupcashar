<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Size extends Model
{
    //product
    public function Product()
    {
        return $this->belongsTo('App\Product');
    }
     #card for size
     public function Cards()
     {
         return $this->hasMany('App\Card');
     }
    
}
