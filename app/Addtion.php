<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Addtion extends Model
{
    //
    protected $fillable = ['name_ar', 'name_en', 'category_id'];
    //shops
    #addtion for products
    public function ProductAddtions()
    {
        return $this->hasMany('App\Product_addtion');
    }
    //Category
    public function Category()
    {
        return $this->belongsTo('App\Category');
    }
}
