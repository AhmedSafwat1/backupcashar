<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    //Category
    public function Category()
    {
        return $this->belongsTo('App\Category');
    }
    #shop
    public function Shop()
    {
        return $this->belongsTo('App\Shop');
    }
    //Sizes
    public function Sizes()
    {
        return $this->hasMany('App\Size');
    }
    #addtion for products
    public function ProductAddtions()
    {
        return $this->hasMany('App\Product_addtion');
    }
    #favourit for products
    public function Favourits()
    {
        return $this->hasMany('App\Favourit');
    }
}
